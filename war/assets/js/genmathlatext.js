jQuery.noConflict();

jQuery(document).ready(function(){

//CKEDITOR.replace( 'editable');

    jQuery("#ch-list-sortable").sortable({
        placeholder: "ui-ch-list-item-highlight",
        refreshPositions: true,
        scroll: true,
      scrollSensitivity: 40,
      scrollSpeed: 40,
      cursor: 'move',
      tolerance: 'pointer',
      axis: 'y',
      opacity: 0.8
    });

    jQuery("#sortable").sortable({
        placeholder: "ui-state-highlight"
    });

	jQuery('.scrollspy-chapterlist').slimScroll({
        height: '300px',
        size: '5px',
        color: '#999999',
        distance: '0px',
        start:'.scrollspy-chapterlist'
    });
	jQuery(".navbar").show();


    jQuery("#btnInline").click(function () {
        // CKEDITOR.replace( 'editable' );


         var editor_data = CKEDITOR.instances.editable.getData();

         alert(editor_data);

        CKEDITOR.instances.editableinline.setData(editor_data);

    //     jQuery('.editor-active').hide();
    //     jQuery('.editor-unactive').show();

    //     CKEDITOR.disableAutoInline = true;
    // CKEDITOR.inline( 'editableinline' );

    //jQuery('#editBoxUninline').hide();

    });

    jQuery("#btn-gen-math-jax").click(function () {
        var getTextGen = $("#input-Gen-Math-Latext").val();
        $("#display-math-jax").html(getTextGen);
         MathJax.Hub.Queue(["Typeset", MathJax.Hub, "display-math-jax"]);
    });

    jQuery("#btnNewChapter").click(function () {
        
        $('#myModal').modal({
            keyboard: false,
            backdrop: true,
            show:true

        });
        
        $('#myTab a[href="#home"]').tab('show');

    });

	jQuery("#btnImportChapter").click(function () {
		var getSizeOfTOC = jQuery("#ch-list-sortable li").size();
		
        var insertContentTB = 	"<li style='display:none;' class='ui-ch-list-item'>"+
                                "<div style='position:relative; float:right;'>"+
                                "<i class='icon-align-justify icon-white'></i>"+
                                "</div>"+
                                "<p style='width=230px;'>"+
                                "<span>"+(getSizeOfTOC+1)+". </span>"+
                                "<span>"+ "Chapter 3  Points, lines and planes" + "</span>"+
                                "</p>"+
                                "</li>";

        jQuery("#ch-list-sortable").append(insertContentTB);

        
        

        jQuery("#ch-list-sortable li:eq("+(getSizeOfTOC)+")").fadeIn();

        var getChapterListHeight = jQuery("#ch-list-sortable").height();

        jQuery('.scrollspy-chapterlist').slimScroll({ scrollTo: getChapterListHeight });
        //jQuery("#tbChapterList tbody tr:eq(6)").fadeIn();
	});

	jQuery(document).on('click', 'tr', function(){

		jQuery(".select-item").removeClass('info select-item');//ลบตัวก่อนหน้าก่อนถ้ามีการ click hilight ตัวอื่นมาก่อนแล้ว
		jQuery(this).addClass('info select-item');
		//alert(jQuery(this).find("td:eq(0)").text());
	});

    jQuery(document).on('click', '#btn-fraction', function(){
        jQuery("#mathInput").val("aaa");
        alert("AAAA");
        //alert(jQuery(this).find("td:eq(0)").text());
    });

    jQuery(document).on('keyup', '#txt-math-input', function(){

        //alert("AAAA");
        var getMathInput = $("#txt-math-input").val();
        console.log(getMathInput);
        $("#math-live-preview").html(getMathInput);
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, "math-live-preview"]);        
        //alert(jQuery(this).find("td:eq(0)").text());
    });


});

