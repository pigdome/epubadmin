/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */


CKEDITOR.editorConfig = function( config ) {


// config.toolbar = [
// 	{ name: 'document', groups: [ 'document', 'doctools' ], items: [ 'Source','NewPage', 'Preview', 'Print', '-', 'Templates' ] },
// 	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
// 	{ name: 'editing', groups: [ 'find'], items: [ 'Find', 'Replace'] },
// 	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
// 	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
// 	{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak','math' ] },
// 	'/',
// 	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
// 	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
// 	{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
// 	{ name: 'others', items: [ '-' ] },
// 	{ name: 'about', items: [ 'About' ] }
// ];

config.toolbar = [
 	{ name: 'document', groups: [ 'document', 'doctools' ], items: [ 'Source','Preview'] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', '-', 'Undo', 'Redo' ] },
	{ name: 'editing', groups: [ 'find'], items: [ 'Find', 'Replace'] },
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	'/',
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak','math' ] },
    { name: 'media', items: [ 'epubimg', 'epubvideo','epubaudio' ] },
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }
];

config.font_names =
            'Arial/Arial, Helvetica, sans-serif;' +
            'Comic Sans MS/Comic Sans MS, cursive;' +
            'Courier New/Courier New, Courier, monospace;' +
            'Georgia/Georgia, serif;' +
            'Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;' +
            'Tahoma/Tahoma, Geneva, sans-serif;' +
            'Times New Roman/Times New Roman, Times, serif;' +
            'Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;' +
            'TH Saraban New/TH Saraban New;' + /* here is your font */
            'Verdana/Verdana, Geneva, sans-serif';
    //config.uiColor = '#AADC6E';
    config.extraPlugins = 'math,epubimg,epubvideo,epubaudio';
    config.height = '280px';
    //config.contentsCss = 'bootstrap.css';

    config.allowedContent = true;
};
