var Connect = (function($){

    var servletUrl = "/admin/AdminServlet";

    var get = function(req,doneCallback,failCallback,alwaysCallback) {
    	$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  async: false,
			  dataType: 'json',
			  data: {
			  	       req : req
			        }
		})
		.done(function(data) {
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() {
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
    };

    var getById = function(req,itemId,doneCallback,failCallback,alwaysCallback) {

    	$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  async: false,
			  dataType: 'json',
			  data: {
			  	       req : req,
                id : itemId
			        }
		})
		.done(function(data) {
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() {
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
    };


    var update = function(req,itemId,item,doneCallback,failCallback,alwaysCallback) {

    	$.ajax({
			  type: 'POST',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  async: false,
			  data: {
			  	       req : req,
                       item : JSON.stringify(item),
                       id : itemId
			        }
		})
		.done(function(data) {
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() {
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
    };

    var remove = function(req,itemId,doneCallback,failCallback,alwaysCallback) {

    	$.ajax({
			  type: 'POST',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  async: false,
			  data : {
			  	        req :req,
			  	        id : itemId
			         }
		})
		.done(function(data) {
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() {
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
    };

    var search = function(req,text,doneCallback,failCallback,alwaysCallback) {

    	$.ajax({
			  type: 'POST',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  async: false,
			  dataType: 'json',
			  data : {
			  	        req :req,
			  	        text : text
			         }
		})
		.done(function(data) {
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() {
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
    };

    return {
        get : function(req,doneCallback,failCallback,alwaysCallback){
             get(req,doneCallback,failCallback,alwaysCallback);
        },
        update : function(req,itemId,item,doneCallback,failCallback,alwaysCallback){
        	update(req,itemId,item,doneCallback,failCallback,alwaysCallback);
        },
        remove : function(req,itemId,doneCallback,failCallback,alwaysCallback){
        	remove(req,itemId,doneCallback,failCallback,alwaysCallback);
        },
        search : function(req,text,doneCallback,failCallback,alwaysCallback){
        	search(req,text,doneCallback,failCallback,alwaysCallback);
        },
        getById : function(req,itemId,doneCallback,failCallback,alwaysCallback){
          getById(req,itemId,doneCallback,failCallback,alwaysCallback);
        }
    };

})(jQuery);
