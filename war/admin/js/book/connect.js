var BookConnect = (function($){


	return {
		list : function(doneCallback,failCallback,alwaysCallback) {
			var req = "listBook" ;
			Connect.get(req,doneCallback,failCallback,alwaysCallback);
		},
		getBookById : function(itemId,doneCallback,failCallback,alwaysCallback){
            var req = "getBookById" ;
			Connect.getById(req,itemId,doneCallback,failCallback,alwaysCallback);
		},
		remove : function(itemId,doneCallback,failCallback,alwaysCallback){
			var req = "deleteBook";
			Connect.remove(req,itemId,doneCallback,failCallback,alwaysCallback);
		},
		update : function(itemId,data,doneCallback,failCallback,alwaysCallback){
            var req = "updateBook";
            Connect.update(req,itemId,data,doneCallback,failCallback,alwaysCallback);
		}
	};

})(jQuery);
