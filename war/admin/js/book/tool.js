 var BookTool = (function($){

 	var $el = {
 		items : $("#items"),
 		bookstemplate : $("#books-template"),
 		showcontentBook : $("#showcontentBook"),
    updateBookTemplate : $("#update-book-template"),
    showupdate : $("#editForm"),
    listBook : $("#btnBook"),
  };



  var show = function(jsonObj) {
   var templateSources = $el.bookstemplate.html();
   var template = Handlebars.compile(templateSources);
   $el.showcontentBook.empty();
   $.each(jsonObj,function(index,element){
    var html = template(element);
    $el.showcontentBook.append(html);
  });
   $('a[rel*=leanModal]').leanModal({ top : 50, closeButton: ".modal_close" });

   onClickEdit();
   onClickDelete();
 };

 var onClickEdit = function(){

   $('.edit').click(function(event){

     event.preventDefault();
     var publication_id = this.id;
     BookConnect.getBookById(publication_id,function(data){

      var time= new Date(parseInt(data.dateCreated)*1000);
      var month = parseInt(time.getMonth())+1;
      data.dateCreated = String(time.getDate()+"/"+month+"/"+time.getFullYear());

      var templateUpdateSources = $el.updateBookTemplate.html();
      var templateUpdate = Handlebars.compile(templateUpdateSources);
      var html = templateUpdate(data);
      $el.showupdate.empty();
      $el.showupdate.append(html);

      onClickUpdate(data);
    });
   });
  };

    var onClickUpdate  = function(data){

      $("#book-update").click(function(event){
       data.title = $("#book-title").val();
       data.authorId = $('#book-authorId').val();
       data.authors = new Array( $('#book-author').val() );
       data.authorsTypes = new Array( $('#book-authorTypes').val() );
       data.coverUrl = $('#book-coverUrl').val();
       data.dateCreated = convertToSec( $('#book-date').val() );
             //data.description = $('#book-desc');
             //data.discipines = $('#book-disc').val();
             data.grades = new Array($('#book-grade').val());
             data.mediaTypes = new Array ( $('#book-media').val() );
             data.otherTitles = new Array ( $('#book-otherTitle').val() );
             data.publicationGroupId = $('#book-publicationGroup').val();
             data.resourceTypes = new Array($('#book-resourceType').val());
             data.status = $('#book-themeId').val();
             data.type  = $('#book-type').val();
             data.version = $('#book-version').val();
             data.versionDateCreated = $('#book-version-date').val();

             BookConnect.update(data.id,data,function(){

              $el.listBook.click();});
           });
    };


    var onClickDelete = function(){
     $(".delete").click(function(event){
      disableAllControll();
      event.preventDefault();
      var publication_id = this.id;

      BookConnect.remove(publication_id,
        function(){$el.listBook.click()},
        function(){},
        function(){
          $el.listBook.click();
        }

        );
    });
   };
   





   var disableAllControll = function(){

    //Display spinner
    $('#btn-fade').click();
    var opts = {
            lines: 13, // The number of lines to draw
            length: 30, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#fafafa', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '200px', // Top position relative to parent
            left: '270px' // Left position relative to parent 
          };
          var target = document.getElementById('spinner');
          var spinner = new Spinner(opts).spin(target);

    };

    var  convertToSec =function(input){
    
    var date = input.split("/");
    var day = parseInt(date[0]);
    var month = parseInt(date[1]);
    var year = parseInt(date[2]);
    if((!isNaN(day))&&(!isNaN(month))&&(!isNaN(year)))
    {
      var time= new Date();
      month = month-1;
      time.setFullYear(year,month,day);
      return parseInt(time.getTime()/1000)
    }
    return null;
  };



        var showFromSearch = function(data,StrSearch){
         var arrayTemp = new Array();
         $el.showcontentBook.empty();
         $.each(data[0],function(index,element){
           var row = searchFromString( element,StrSearch );
           if( row != null )
            arrayTemp.push(row);
        });

         show(arrayTemp);
       };

       var searchFromString = function(rowJson,StrSearch){

		if( String(rowJson.title).indexOf( StrSearch.trim() ) >= 0 )//TO DO HERE!!
     return rowJson;
   if( rowJson.books == StrSearch )
     return rowJson;
   if( rowJson.alias == StrSearch )
     return rowJson;
   return null;
 };

 return {
   show : function(data){
    show(data);
  },
  showFromSearch : function(data,StrSearch){
    showFromSearch(data,StrSearch);
  }
};

})(jQuery);

