var Tool = (function($){

  var $el = {
    searchBox : $('#search'),
    searchBtn : $("#btnSearch"),
    listBookBtn : $("#btnBook"),
    listAuthorBtn : $("#btnAuthor"),
    listBook : $("#listBook"),
    listAuthor : $("#listAuthor")
  };




  var init = function() {
    onClickSearch();
    onClickListAuthor();
    onClickListBook();
  };

  var onClickSearch = function() {

    $el.searchBtn.on("click",function(event){
      event.preventDefault();
      var search =  $('input[name=select-search]:checked').val();
      if( search == "book" )
      {
        enableBookTable();
        BookConnect.list(
         function(data){ BookTool.showFromSearch(data,$el.searchBox.val()); },
         function(){ console.log("Server not respone");}
         );
      }

      if( search == "author" )
      {
       enableAuthorTable();
       AuthorConnect.list(
         function(data){ AuthorTool.showFromSearch(data,$el.searchBox.val()); },
         function(){ console.log("Server not respone");}
         );
     } 
     
   });
  };

  var onClickListAuthor = function() {

    $el.listAuthorBtn.click(function(event){
      enableAuthorTable();
      event.preventDefault();
      AuthorConnect.list(
       function(data){ AuthorTool.show(data); },
       function(){ console.log("server error"); }

       );
    });
  };

  var onClickListBook = function() {

    $el.listBookBtn.click(function(){
      enableBookTable();
      event.preventDefault();
      BookConnect.list(
        function(data){ BookTool.show(data[0]); },
        function(){console.log("server error"); }

        );
    });
  };

  var enableBookTable = function(){
    $el.listBook.removeClass("hidden").addClass("visible");
    $el.listAuthor.removeClass("visible").addClass("hidden");
  };

  var enableAuthorTable = function(){
    $el.listAuthor.removeClass("hidden").addClass("visible");
    $el.listBook.removeClass("visible").addClass("hidden");
  };

  



  return {
    init : function(){
      init();
    }
    
  };

})(jQuery);
