var AuthorTool = (function($){

	var $el = {
		items : $("#items"),
		authorstemplate : $("#authors-template"),
		showcontentAuthor : $("#showcontentAuthor"),
		updateAuthorTemplate : $("#update-author-template"),
		showupdate : $("#editForm"),
		listAuthor : $("#btnAuthor")
	};




	var show = function(jsonObj) {
        console.log(jsonObj);
		var templateSources = $el.authorstemplate.html();
		var template = Handlebars.compile(templateSources);
		$el.showcontentAuthor.empty();
		$.each(jsonObj,function(index,element){
			var html = template(element);
			$el.showcontentAuthor.append(html);
		});
		$('a[rel*=leanModal]').leanModal({ top : 50, closeButton: ".modal_close" });
		onClickEdit();
		onClickDelete();
    };
        
    var onClickEdit = function(){
        
        $('.edit').click(function(event){
            event.preventDefault();
			var author_id = this.id;
			AuthorConnect.getAuthorById(author_id,function(data){

			var time= new Date(parseInt(data.dateOfMember)*1000);
			var month = parseInt(time.getMonth())+1;
			data.dateOfMember = String(time.getDate()+"/"+month+"/"+time.getFullYear());


			var templateUpdateSources = $el.updateAuthorTemplate.html();
			var templateUpdate = Handlebars.compile(templateUpdateSources);
			var html = templateUpdate(data);
			$el.showupdate.empty();
			$el.showupdate.append(html);
                
			onClickUpdate(data);
			});
	    });
    };
		
	var onClickUpdate = function(data){
        
        $("#author-update").click(function(event){
             data.displayName = $("#author-name").val();
             data.about = $('#author-about').val();
             data.dateOfMember = convertToSec( $('#author-dateOfMember').val() );
             data.email = $('#author-email').val();
             data.identityProvider = $('#author-idProvider').val();
             data.publicationCount = new Number($('#author-publicationCount').val());
             //data.userInfo = $("#author-info").val();
             
             AuthorConnect.update(data.id,data,function(){
             	
             	$el.listAuthor.click();
             });

          });
	}; 	

			  
		
	var onClickDelete = function(){
       
       $(".delete").click(function(event){
            disableAllControll();
            event.preventDefault();
			var author_id = this.id;
			AuthorConnect.remove(author_id,
				function(){$el.listAuthor.click();},
		        function(){console.log("no");},
		        function(){
		         console.log("ok");
		         $el.listAuthor.click();
		        }
			);
		});
	};

		
	


	
    var disableAllControll = function(){
     
    //Display spinner
    $('#btn-fade').click();
    var opts = {
            lines: 13, // The number of lines to draw
            length: 30, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#fafafa', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '200px', // Top position relative to parent
            left: '270px' // Left position relative to parent 
          };
    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target);
  
  };
	







	var showFromSearch = function(data,StrSearch){

		var arrayTemp = new Array();
		$el.showcontentAuthor.empty();
		$.each(data,function(index,element){
			var row = searchFromString( element,StrSearch );
			if( row != null )
				arrayTemp.push(row);
		});
		show(arrayTemp);
	};

	var searchFromString = function(rowJson,StrSearch){

		if( String(rowJson.displayName).indexOf( StrSearch.trim() ) >= 0 )
			return rowJson;
		if( rowJson.author == StrSearch )
			return rowJson;
		if( rowJson.SubjectGroups == StrSearch )
			return rowJson;
		if( rowJson.level == StrSearch )
			return rowJson;
		if( rowJson.date == StrSearch )
			return rowJson;
		return null;
	};

	var  convertToSec =function(input){
		
		var date = input.split("/");
		var day = parseInt(date[0]);
		var month = parseInt(date[1]);
		var year = parseInt(date[2]);
		if((!isNaN(day))&&(!isNaN(month))&&(!isNaN(year)))
		{
			var time= new Date();
			month = month-1;
			time.setFullYear(year,month,day);
			return parseInt(time.getTime()/1000)
		}
		return null;
	};

	return {
		show : function(data){
			show(data);
		},
		showFromSearch : function(data,StrSearch){
			showFromSearch(data,StrSearch);
		}

	};
})(jQuery);
