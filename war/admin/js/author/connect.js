var AuthorConnect = (function($){


	return {
		list : function(doneCallback,failCallback,alwaysCallback) {
			var  req = "listAuthor" ;
			Connect.get(req,doneCallback,failCallback,alwaysCallback);
		},
		getAuthorById : function(itemId,doneCallback,failCallback,alwaysCallback){
            var req = "getAuthorById" ;
			Connect.getById(req,itemId,doneCallback,failCallback,alwaysCallback);
		},
		remove : function(itemId,doneCallback,failCallback,alwaysCallback){
			var req = "deleteAuthor";
			Connect.remove(req,itemId,doneCallback,failCallback,alwaysCallback);
		},
		update : function(itemId,data,doneCallback,failCallback,alwaysCallback){
            var req = "updateAuthor";
            Connect.update(req,itemId,data,doneCallback,failCallback,alwaysCallback);
		}
	};

})(jQuery);
