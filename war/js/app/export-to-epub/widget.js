var ExportToEpubWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		exportToEpubButton: $("#btnExportToEpub"),
		epubDownloadLinkContainer: $("#epubDownloadLinkContainer"),
		epubDownloadLink: $("#epubDownloadLink"),

		publicationId: $("#publicationId"),
	};
	
	var gcsRootPath="http://commondatastorage.googleapis.com/ipst-epub/";
	
	var exportToEpub=function() {

    	EpubPackageWidget.disableAllControls();
    	// save currently open page, if there is one
    	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

    		EpubPackageEndpoint.genEpub($el.publicationId.val(), function(signedEpubUrl) {
    			//showDownloadLink(signedEpubUrl);
    			//window.open(signedEpubUrl);
    			//window.open(signedEpubUrl, "_blank");
    			window.location.href=signedEpubUrl; // http://stackoverflow.com/questions/12365534/launch-download-in-the-same-tab-without-opening-new-tab-or-window-in-javascript
    		}, function() {
        		alert("ไม่สามารถนำออกเป็นไฟล์ ePub ได้ กรุณาลองอีกครั้ง")
    		}, function() {
        		EpubPackageWidget.enableAllControls();
    		});

    	}, function() {
    		alert("ไม่สามารถนำออกเป็นไฟล์ ePub ได้ กรุณาลองอีกครั้ง")
    		EpubPackageWidget.enableAllControls();
    	}, function() {
    	});		
    	
	};
	
	var init=function() {
		$el.exportToEpubButton.on("click", function() {
			exportToEpub();
		});
	};
	
	return {
		init: function() {
			init();
		}
	};
	
})(jQuery);