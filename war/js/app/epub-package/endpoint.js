var EpubPackageEndpoint=(function($) {
	
	var baseUrl="/epubpackageendpoint";
	
	// binding to UIs and caching elements
	var $el = {
		// TODO: เธ•เธญเธ�เธ�เธตเน�เธขเธฑเธ�เน�เธกเน� support versioning fix เน�เธงเน� เน€เธ�เน�เธ� "v1" เธ�เธฑเธ� "true" (เน�เธ� PublicationConnector.java's insert) เน�เธ�เธ�เน�เธญเธ�
		//version: $(""), // v1, v2, v3, etc. 
		//isLatestVersion: $(""), // เน�เธ�เน�เธ•เธญเธ� select เน€เธ�เธ�เธฒเธฐ latest version 

		// TODO: เธ•เธญเธ�เธ�เธตเน�เธขเธฑเธ�เน�เธกเน�เน�เธขเธ� textbook/concept   fix เน�เธงเน�เน€เธ�เน�เธ� textbook เธ�เน�เธญเธ� เธ•เธญเธ� insert
		//String type; // textbook, concept

		//String status; // เธ•เธญเธ� insert default เน€เธ�เน�เธ� "in progress"
		
		authorId: $("#authorId"),
		selectedCover: $("#preview-book-cover-create-new img"), 
	    title: $(".input-book-title"),
	    otherTitles: $("#otherTitles"),
	    authorsTypes_authors: $("#authorsTypes_authors"),
	    mediaTypes: $("#mediaTypes"),
	    resourceTypes: $("#resourceTypes"),
	    disciplines: $("#disciplines"),
	    grades: $("#grades"),
	    description: $("#description"),
	    subjectHeadings: $("#subjectHeadings")
	};
	
	var insert = function(servletUrl, req, epubPackage, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				epubPackage: JSON.stringify({
					publicationId: epubPackage.publicationId,		
					
					title: epubPackage.title,
					identifier: epubPackage.identifier,
					language: epubPackage.language,
					
					itemIds: epubPackage.itemIds,
					itemProperties: epubPackage.itemProperties,
					itemHrefs: epubPackage.itemHrefs,
					itemMediaTypes: epubPackage.itemMediaTypes,

					mediaItemIds: epubPackage.mediaItemIds,
					mediaItemHrefs: epubPackage.mediaItemHrefs,
					mediaItemMediaTypes: epubPackage.mediaItemMediaTypes,

					itemrefIdRefs: epubPackage.itemrefIdRefs,
					itemrefLinears: epubPackage.itemrefLinears,
					
					tocHtmlString: epubPackage.tocHtmlString, // which is { value: ... }
					navHtmlString: epubPackage.navHtmlString, // which is { value: ... }
				})
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};
	
	var update = function(servletUrl, req, epubPackage, doneCallback, failCallback, alwaysCallback) {
		
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: req,
				epubPackage: JSON.stringify({
					id: epubPackage.id,
					publicationId: epubPackage.publicationId,					

					title: epubPackage.title,
					identifier: epubPackage.identifier,
					language: epubPackage.language,
					
					itemIds: epubPackage.itemIds,
					itemProperties: epubPackage.itemProperties,
					itemHrefs: epubPackage.itemHrefs,
					itemMediaTypes: epubPackage.itemMediaTypes,
					
					mediaItemIds: epubPackage.mediaItemIds,
					mediaItemHrefs: epubPackage.mediaItemHrefs,
					mediaItemMediaTypes: epubPackage.mediaItemMediaTypes,
					
					itemrefIdRefs: epubPackage.itemrefIdRefs,
					itemrefLinears: epubPackage.itemrefLinears,
					
					tocHtmlString: epubPackage.tocHtmlString,
					navHtmlString: epubPackage.navHtmlString,
				})
			} 
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() { 
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });			
	};

	var get = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'json'
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};

	var genEpub = function(servletUrl, doneCallback, failCallback, alwaysCallback) {
		$.ajax({
			  type: 'GET',
			  contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			  url: servletUrl,
			  dataType: 'text' // เธชเธณเธ�เธฑเธ�เธกเธฒเธ�เธ•เน�เธญเธ�เธ•เธฃเธ�เธ�เธฑเธ�เธ�เธฑเน�เธ� servlet เน�เธกเน�เธ�เธฑเน�เธ�เน€เธ�เน�เธฒ .fail
		})
		.done(function(data) { 
			if(doneCallback)
				doneCallback(data);
		})
	    .fail(function() { 
	    	if(failCallback)
	    		failCallback();
	    })
	    .always(function() {
	    	if(alwaysCallback)
	    		alwaysCallback();
	    });
	};
	//My Code ==============================================
	var increaseChapter = function(servletUrl, publicationId, titlename) {
		$.ajax({
			type: 'POST',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			url: servletUrl,
			data: { 
				req: "increase",
				publicationId: publicationId,
				title: titlename
			} 
		});		
	};
	//My Code ==============================================
	return {
		getByPublicationId: function(publicationId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl + "?req=get&publicationId="+publicationId;
			get(servletUrl, doneCallback, failCallback, alwaysCallback);
		},
		genEpub: function(publicationId, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl + "?req=genEpub&publicationId="+publicationId;
			genEpub(servletUrl, doneCallback, failCallback, alwaysCallback);			
		},
		insert: function(epubPackage, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="insert";
			insert(servletUrl, req, epubPackage, doneCallback, failCallback, alwaysCallback);
		},
		update: function(epubPackage, doneCallback, failCallback, alwaysCallback) {
			var servletUrl=baseUrl;
			var req="update";
			update(servletUrl, req, epubPackage, doneCallback, failCallback, alwaysCallback);
		},
		remove: function(callback) {
			
		},
		//My Code======================================
		increase: function(publicationId,titlename) {
			var servletUrl = "chapterendpoint";
			increaseChapter(servletUrl,publicationId,titlename);
		}
		//My Code======================================
	};
	
})(jQuery);
