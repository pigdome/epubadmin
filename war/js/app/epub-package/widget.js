var EpubPackageWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		bookTitle: $("#book-name > h5"),
		bookCover: $("#bookCover"),
		bookInfo: $("#bookInfo"),
		navBar: $(".navbar"),
		
		createChapterButton: $("#btn-create-chapter"),
		createSectionButton: $("#btn-create-section"),
		createCopyrightButton: $("#btn-create-copyright"),
		createDedicationButton: $("#btn-create-dedication"),
		createForwardButton: $("#btn-create-forward"),
		createPrefaceButton: $("#btn-create-preface"),
		
		createChapterConfirmButtonSelector: "#btn-confirm-create-chapter",
		createSectionConfirmButtonSelector: "#btn-confirm-create-section",
		createCopyrightConfirmButtonSelector: "#btn-confirm-create-copyright",
		createDedicationConfirmButtonSelector: "#btn-confirm-create-dedication",
		createForwardConfirmButtonSelector: "#btn-confirm-create-forward",
		createPrefaceConfirmButtonSelector: "#btn-confirm-create-preface",
		
		createChapterCancelButtonSelector: "#btn-cancel-create-chapter",
		createSectionCancelButtonSelector: "#btn-cancel-create-section",
		createCopyrightCancelButtonSelector: "#btn-cancel-create-copyright",
		createDedicationCancelButtonSelector: "#btn-cancel-create-dedication",
		createForwardCancelButtonSelector: "#btn-cancel-create-forward",
		createPrefaceCancelButtonSelector: "#btn-cancel-create-preface",
		
		createChapterModal: null,  // can't initialize here because it's not in the DOM until page load done..
		createChapterModalSelector: "#modal-create-chapter",
		createSectionModal: null, 
		createSectionModalSelector: "#modal-create-section", 
		createCopyrightModal: null,
		createCopyrightModalSelector: "#modal-create-copyright",
		createDedicationModal: null,
		createDedicationModalSelector: "#modal-create-dedication",
		createForwardModal: null,
		createForwardModalSelector: "#modal-create-forward",
		createPrefaceModal: null,
		createPrefaceModalSelector: "#modal-create-preface",
		
		userEnteredChapterNameSelector: "#txt-chapter-name",
		userEnteredSectionNameSelector: "#txt-section-name",
		
		bookTemplatesList: $("#bookTemplatesList"),
		selectedChapterTemplateNameSelector: "input[name='rb-chapter-template']:checked",
		selectedSectionTemplateNameSelector: "input[name='rb-section-template']:checked",
		selectedCopyrightTemplateNameSelector: "input[name='rb-copyright-template']:checked",
		selectedDedicationTemplateNameSelector: "input[name='rb-dedication-template']:checked",
		selectedForwardTemplateNameSelector: "input[name='rb-forward-template']:checked",
		selectedPrefaceTemplateNameSelector: "input[name='rb-preface-template']:checked",

		editingArea: $("#editingArea"),
		titleSelector: ".ipst-epub-header-desc",
		saveHtmlButton: $("#btnSaveHtml"),
		
		/* เน�เธกเน�เธ•เน�เธญเธ�เน€เธ�เธฅเธตเน�เธขเธ�เน€เธ�เน�เธ� selector เธซเธฃเธญเธ� fix เน�เธ� code เธ�เธฐเน€เธ�เน�เธฒเน�เธ�เธ�เน�เธฒเธขเธ�เธงเน�เธฒเธ�เธฐ
		nestable: $("#nestable"),
		nestableList: $(".dd > .dd-list"),
		nestableContainer: $(".dd"),
		nestableTocContainerParent: $(".slimScrollDiv"),
		*/
		tocContainer: $("#contain-chapter-list"),
		expandAllButton: $("#btn-expand-all"),
		collapseAllButton: $("#btn-collapse-all"),
	};
	
	var bookTemplatesListForTheme1Template=Handlebars.templates["book-templates-list-for-theme1"];
	var bookTemplatesListForTheme2Template=Handlebars.templates["book-templates-list-for-theme2"];
	var bookTemplatesListForTheme3Template=Handlebars.templates["book-templates-list-for-theme3"];
	var tocItemNestableTemplate=Handlebars.templates["toc-item-nestable"];
	var tocSubitemContainerNestableTemplate=Handlebars.templates["toc-subitem-container-nestable"];

	var didInitializeNestable=false;
	
	var loadToc=function(tocHtmlString) {
		if(tocHtmlString==null || tocHtmlString.value=="") {
			// create empty toc
			var $toc=$("<div class='dd' id='nestable' style='height: 100px; overflow: hidden;'> <ol class='dd-list'></ol> </div>");
			$el.tocContainer.append($toc);
		} else { // load existing toc			
			
			$el.tocContainer.append(tocHtmlString.value); // .value because of the Text's datatype

		}
		
		// TODO: เธกเธตเธงเธดเธ�เธตเธ”เธตเธ�เธงเน�เธฒเธ�เธตเน�เธกเธฑเน�เธขเน€เธ�เธตเน�เธข
		if(!didInitializeNestable) {
			onNestableEvents(); // เธ•เน�เธญเธ�เน€เธฃเธตเธขเธ�เธซเธฅเธฑเธ�เธ�เธฒเธ�เธกเธต element #nestable เน�เธฅเน�เธงเน€เธ—เน�เธฒเธ�เธฑเน�เธ� เน�เธฅเธฐ initialize เน�เธ�เน�เธ•เธญเธ�เน€เธชเธฃเน�เธ� page load (เธ—เธณเน�เธ�เน�เธ�เธฃเธฑเน�เธ�เน€เธ”เธตเธขเธง)
			didInitializeNestable=true;
			
			loadLastSavedPage(); // load เธซเธ�เน�เธฒเธ—เธตเน�เน€เธ�เธดเธ” save เน�เธงเน�เธ�เธฃเธฑเน�เธ�เธชเธธเธ”เธ—เน�เธฒเธข (เธ•เธฃเธ�เธ�เธงเน�เธฒเธ�เธฑเน�เธ�เธ�เธทเธญ เธซเธ�เน�เธฒเธ—เธตเน� highlight เธ�เน�เธฒเธ�เน�เธงเน�เธญเธขเธนเน�เน�เธ� EpubPackage entity เธญเธฒเธ�เธ�เธฐเน€เธ�เน�เธ�เธซเธ�เน�เธฒเธ—เธตเน�เธขเน�เธฒเธขเธ•เธณเน�เธซเธ�เน�เธ�เน�เธ�เธฅเน�เธฒเธชเธธเธ”เธ�เน�เน�เธ”เน�)
		}
	};
	
	var loadLastSavedPage=function() {
		var $previouslySelectedItem=$(".dd-selected-item");
		if($previouslySelectedItem.length>0) {
			var htmlHref=$previouslySelectedItem.parent().attr("data-html-href");
			loadHtmlContent(htmlHref);			
		}
	};
	
	var storeEpubPackageInEditingAreaElement=function(epubPackage) {
		$el.editingArea.attr("data-epub-package", JSON.stringify(epubPackage));		
	};
	
	var getEpubPackage=function(publicationId) {
		EpubPackageEndpoint.getByPublicationId(publicationId, 
			function(epubPackage) {

				storeEpubPackageInEditingAreaElement(epubPackage);
				loadToc(epubPackage.tocHtmlString);

			}, function() {
				
				EpubPackageEndpoint.insert({ publicationId: publicationId, title: $el.bookTitle.text(), tocHtmlString: { value: "" } , navHtmlString: { value: "" } }, 
					function(insertedEpubPackage) {

						storeEpubPackageInEditingAreaElement(insertedEpubPackage);
						loadToc(insertedEpubPackage.tocHtmlString);

					}
				);
			}
		);
	};
	
	var postProcessHtmlFile=function() {
        var $content=$el.editingArea.find("> div");
        
		// revert background path and style (if they exist) to the original
        var originalStyle=$content.attr("data-original-style");
        $content.attr("style", originalStyle);        

		// change system image (e.g. backgrounds, lines, placeholders) paths
		var $systemImages=$content.find(".ipst-epub-system-image");
		if($systemImages.length>0) {
			$systemImages.each(function(index, image) {
				$(image).attr("src", $(image).attr("data-original-url"));
			});			
		}

		// change media paths to local package
		var $images=$content.find(".myCKImage");
		if($images.length>0) {
			MediaEndpoint.loadImageAsync($images);
		}

		var $videos=$content.find(".myCKVideo");
		if($videos.length>0) {
			MediaEndpoint.loadVideoAsync($videos);
		}

		var $audios=$content.find(".myCKAudio");
		if($audios.length>0) {
			MediaEndpoint.loadAudioAsync($audios);
		}
	};
	
	var renderDownloadedHtmlContent=function($content) {
		
		CKEditorWidget.deactivateAll();
        $el.editingArea.empty();
        
        // remove src of media files to prevent broken-source visualization
        $content.find(".myCKImage, .myCKVideo, .myCKAudio").removeAttr("src");
        $content.find(".ipst-epub-system-image").removeAttr("src");
        
        $el.editingArea.append($content);
        $el.editingArea.find("> div").hide();
        $el.editingArea.find("> div").fadeIn();

        CKEditorWidget.activateAll();

        // window.setTimeout(postProcessHtmlFile, 3000);
        // Previously we use a timer to delay post processing, but it seems to be related to CKEditor which is not yet ready to be used.
        // So handle such an event should solve the problem.
        //
        // *** IMPORTANT: Currently there is only one CKEditor inside the page. If multiple editors are needed, implementation of CKEditorWidget.onReady has to be changed. ***
        // 
        // See http://docs.ckeditor.com/#!/api/CKEDITOR-event-instanceReady
        // See http://stackoverflow.com/questions/19805338/adding-event-handlers-globally-to-all-ckeditor-instances
        CKEditorWidget.onReady(postProcessHtmlFile);
	};

	var loadHtmlContent=function(htmlHref, doneCallback, failCallback, alwaysCallback) {
		
		disableAllControls();
		
		HtmlDownloadEndpoint.downloadContent(htmlHref, function($content) {
			
			renderDownloadedHtmlContent($content);
			
			if(doneCallback)
				doneCallback();

		}, function() {
			alert("เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เน�เธชเธ”เธ�เน€เธ�เธทเน�เธญเธซเธฒเน�เธ”เน� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เน�เธซเธกเน�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�");			
			if(failCallback)
				failCallback();
		}, function() {
			if(alwaysCallback)
				alwaysCallback();
			enableAllControls();				
		});
	};
	
	var setup=function(publicationId) {
		console.log(publicationId);
		PublicationEndpoint.getById(publicationId, function(publication) { // เน�เธ�เน�เน�เธ�เน� publicationId เธ�เน�เธ�เธญ เน�เธกเน�เธ�เธณเน€เธ�เน�เธ�เธ•เน�เธญเธ�เธฃเธนเน� version เน€เธ�เธฃเธฒเธฐ publicationId เธ�เธทเธญ key เธ�เธญเธ� Publication entity เธชเน�เธงเธ�เธ•เธญเธ�เธ—เธณ versioning เธ•เน�เธญเธ�เน�เธ�เน� publicationGroupId
			// เธ”เธถเธ� publication entity เธกเธฒเน€เธ�เธทเน�เธญ set book title เธ�เธฑเธ� cover
			$el.bookInfo.data("book", JSON.stringify(publication));
			$el.bookTitle.text(publication.title);
			$el.bookCover.data("url", publication.coverUrl);
			PublicationEndpoint.loadImageAsync($el.bookCover);
			
			getEpubPackage(publicationId); // เธ•เน�เธญเธ�เน€เธฃเธตเธขเธ�เธ•เธญเธ�เน�เธ”เน� Publication entity เธกเธฒเน�เธฅเน�เธง เน€เธ�เธทเน�เธญเธ�เธฃเธ“เธตเธ—เธตเน�เธขเธฑเธ�เน�เธกเน�เน€เธ�เธขเธกเธต EpubPackage entity เธ�เธฐเน�เธ”เน�เธชเธฃเน�เธฒเธ� entity เน�เธซเธกเน� เธ�เธถเน�เธ�เธ•เน�เธญเธ�เน�เธ�เน� publication.title
			
			loadTemplatesForTheme(publication.themeId);
		});
	};
	
	var loadTemplatesForTheme=function(themeId) {
		if(themeId=="theme1") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme1Template()); 
		} else if(themeId=="theme2") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme2Template());
		} else if(themeId=="theme3") {
			$el.bookTemplatesList.append(bookTemplatesListForTheme3Template());			
		}
	};

	var fadeInNewItemAndHighlightIt=function() {
        var $newlyAddedItem=$(".new-item-added");
        $newlyAddedItem.fadeIn();
        var itemListHeight = $(".dd > .dd-list").height();
        $(".dd").slimScroll({ scrollTo: itemListHeight });
        $newlyAddedItem.removeClass("new-item-added");
        
        // unhighlight previously highlighted one, and highlight the new item
        $(".dd-selected-item").removeClass("dd-selected-item");
        $newlyAddedItem.find(".dd-handle").addClass("dd-selected-item");
	};
	
	var addNewSubitemInToc=function(title, nestableTemplate, $parent) {
    	var newItemInToc = { 
    		timestamp: new Date().getTime(), 
    		title: title 
    	};
        $parent.append(nestableTemplate(newItemInToc));
        var serializedListInfo = JSON.stringify($("#nestable").nestable('serialize')); // serializedListInfo เธกเธตเน�เธงเน�เน€เธ�เน�เธ�เธงเน�เธฒเธกเธตเธ�เธฒเธฃเน€เธ�เธฅเธตเน�เธขเธ�เธ•เธณเน�เธซเธ�เน�เธ� (reorder) เธซเธฃเธทเธญเน�เธกเน�
        $(".dd").attr("data-serialize-toc", serializedListInfo);
		
        fadeInNewItemAndHighlightIt();
	};
	
	var addNewItemInToc=function(title, nestableTemplate) {
    	var newItemInToc = { 
    		timestamp: new Date().getTime(), 
    		title: title 
    	};
    	$(".dd > .dd-list").append(nestableTemplate(newItemInToc));
        var serializedListInfo = JSON.stringify($("#nestable").nestable('serialize')); // serializedListInfo เธกเธตเน�เธงเน�เน€เธ�เน�เธ�เธงเน�เธฒเธกเธตเธ�เธฒเธฃเน€เธ�เธฅเธตเน�เธขเธ�เธ•เธณเน�เธซเธ�เน�เธ� (reorder) เธซเธฃเธทเธญเน�เธกเน�
        $(".dd").attr("data-serialize-toc", serializedListInfo);
		
        fadeInNewItemAndHighlightIt();
	};

	var disableAllControls=function() {
		// all buttons
		$("button").attr("disabled", "disabled");
		
		// toc control panel
		$(".dd").find(".dd-handle").each(function() {
            $(this).removeClass("dd-handle");
            $(this).addClass("dd-disable");
            $(this).attr('id', 'TOC-col');
        });
        $('.dd').animate({opacity: 0.5});
	};
	
	var enableAllControls=function() {
		// all buttons
		$("button").removeAttr("disabled");		
		
		// toc control panel
		$(".dd").find(".dd-disable").each(function(){
            $(this).removeClass("dd-disable");
            $(this).addClass("dd-handle");
        });
        $('.dd').animate({opacity: 1.0});
        
        // TODO: enable offline support only after fixing bug of not being able to load media files
		//OfflineSupportWidget.activate();
	};
		
	var showTemplate=function(bookTemplateName, title) {
		        
    	disableAllControls();
    	// save currently open page, if there is one
    	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

    		// clear old editing area, if it exists
            $el.editingArea.empty();

            // replace editing area with new page's content
            var template=Handlebars.templates[bookTemplateName];
            $el.editingArea.append(template({title: title}));        
            $("."+bookTemplateName).fadeIn();		

            CKEditorWidget.activateAll();

            // save new page
        	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {
        		
        	}, function() {
        		alert("เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เธ�เธฑเธ�เธ—เธถเธ�เธ�เธฒเธฃเน€เธ�เธฅเธตเน�เธขเธ�เน�เธ�เธฅเธ�เน�เธ”เน� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�")        		
        	}, function() {
        		enableAllControls();        		
        	});

    	}, function() {
    		alert("เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เธ�เธฑเธ�เธ—เธถเธ�เธ�เธฒเธฃเน€เธ�เธฅเธตเน�เธขเธ�เน�เธ�เธฅเธ�เน�เธ”เน� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�")
    		enableAllControls();
    	}, function() {
    	});
	};
	
	var showNewChapter=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

		addNewItemInToc(userEnteredTitle, tocItemNestableTemplate);
		
        // set title and fade in the template
        showTemplate(bookTemplateName, userEnteredTitle);
        // create new chapter datastore
			 createChapterData($publicationId,userEnteredTitle); 
	};
	//=====================================================================
	//My code
	var createChapterData=function(publicationID,userEnteredTitle){
		console.log(publicationID);
		console.log(userEnteredTitle);
		EpubPackageEndpoint.increase(publicationID,userEnteredTitle);
	}
	//=====================================================================
	var didTapOnCreateChapterConfirmButton=function() {

        var bookTemplateName = $($el.selectedChapterTemplateNameSelector).val();
        var userEnteredChapterName=$($el.userEnteredChapterNameSelector).val();
        
        $el.createChapterModal.one("hidden", { "userEnteredTitle": userEnteredChapterName , "bookTemplateName": bookTemplateName}, showNewChapter);
        $el.createChapterModal.modal("hide");
	};

	var showNewCopyright=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

		addNewItemInToc(userEnteredTitle, tocItemNestableTemplate);		
        showTemplate(bookTemplateName, "UNTITLED");
	};

	var didTapOnCreateCopyrightConfirmButton=function() {

        var bookTemplateName = $($el.selectedCopyrightTemplateNameSelector).val();

        $el.createCopyrightModal.one("hidden", { "userEnteredTitle": "เธฅเธดเธ�เธชเธดเธ—เธ�เธดเน�", "bookTemplateName": bookTemplateName }, showNewCopyright);
        $el.createCopyrightModal.modal("hide");
	};

	var showNewDedication=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

		addNewItemInToc(userEnteredTitle, tocItemNestableTemplate);		
        showTemplate(bookTemplateName, "UNTITLED");
	};

	var didTapOnCreateDedicationConfirmButton=function() {

        var bookTemplateName = $($el.selectedDedicationTemplateNameSelector).val();

        $el.createDedicationModal.one("hidden", { "userEnteredTitle": "เธ�เธณเธญเธธเธ—เธดเธจ", "bookTemplateName": bookTemplateName }, showNewDedication);
        $el.createDedicationModal.modal("hide");
	};

	var showNewForward=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

		addNewItemInToc(userEnteredTitle, tocItemNestableTemplate);		
        showTemplate(bookTemplateName, "UNTITLED");
	};

	var didTapOnCreateForwardConfirmButton=function() {

        var bookTemplateName = $($el.selectedForwardTemplateNameSelector).val();

        $el.createForwardModal.one("hidden", { "userEnteredTitle": "เธ�เธณเธ�เธดเธขเธก", "bookTemplateName": bookTemplateName }, showNewForward);
        $el.createForwardModal.modal("hide");
	};

	var showNewPreface=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

		addNewItemInToc(userEnteredTitle, tocItemNestableTemplate);		
        showTemplate(bookTemplateName, "UNTITLED");
	};

	var didTapOnCreatePrefaceConfirmButton=function() {

        var bookTemplateName = $($el.selectedPrefaceTemplateNameSelector).val();

        $el.createPrefaceModal.one("hidden", { "userEnteredTitle": "เธ�เธณเธ�เธณ", "bookTemplateName": bookTemplateName }, showNewPreface);
        $el.createPrefaceModal.modal("hide");
	};

	var showNewSection=function(event) {
		var userEnteredTitle=event.data["userEnteredTitle"];
		var bookTemplateName=event.data["bookTemplateName"];

        var chapterSelected=$(".dd-selected-item").parent().parent().parent().is("div");
        var sectionSelected=$(".dd-selected-item").parent().parent().parent().is("li");

        if(chapterSelected) {
            var numSections = $(".dd-selected-item").parent().find(".dd-list").length;
            if(numSections > 0) { // meaning that the list already contains the <ol/> container
            	var $parent=$(".dd-selected-item").parent().find("> .dd-list");
            	addNewSubitemInToc(userEnteredTitle, tocItemNestableTemplate, $parent);
            }
            else { // adding <ol/> container for the first section item
            	var $parent=$(".dd-selected-item").parent();
            	addNewSubitemInToc(userEnteredTitle, tocSubitemContainerNestableTemplate, $parent);
            }
        } else if(sectionSelected) {
        	var $parent=$(".dd-selected-item").parent().parent();
        	addNewSubitemInToc(userEnteredTitle, tocItemNestableTemplate, $parent);
        }

        // expand all items in toc, just to make sure that a new section is shown on toc properly
        $el.expandAllButton.trigger("click");
        
        // set title and fade in the template
        showTemplate(bookTemplateName, userEnteredTitle);
	};
	
	var didTapOnCreateSectionConfirmButton=function() {

        var bookTemplateName = $($el.selectedSectionTemplateNameSelector).val();
        var userEnteredSectionName=$($el.userEnteredSectionNameSelector).val();

        $el.createSectionModal.one("hidden", { "userEnteredTitle": userEnteredSectionName, "bookTemplateName": bookTemplateName }, showNewSection);
        $el.createSectionModal.modal("hide");
	};
	
	var onTemplateEvents=function() {
		
        $el.createChapterButton.on("click", function () {
        	$el.createChapterModal=$($el.createChapterModalSelector); // the element is in the DOM now so we can initialize dynamically 
            $el.createChapterModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createChapterModal.one("click", $el.createChapterConfirmButtonSelector, function () {
            	didTapOnCreateChapterConfirmButton();
            });
            // เน�เธกเน�เธ�เธฑเน�เธ�เธ•เธญเธ�เธ�เธ” cancel เน�เธฅเน�เธงเธ�เธฅเธฑเธ�เธกเธฒเธ�เธ” confirm เน�เธซเธกเน� เธ�เธฐ create เน€เธ�เธดเน�เธฅเน€เธฅเธข เน€เธ�เธฃเธฒเธฐเน€เธ�เน�เธฒ event handler 2 เธ�เธฃเธฑเน�เธ�เธ�เธญเธ� .on
            $el.createChapterModal.on("click", $el.createChapterCancelButtonSelector, function() {
            	$el.createChapterModal.off("click", $el.createChapterConfirmButtonSelector);
            });
        });

        $el.createSectionButton.on("click", function () {
        	$el.createSectionModal=$($el.createSectionModalSelector);
            $el.createSectionModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createSectionModal.one("click", $el.createSectionConfirmButtonSelector, function () {
            	didTapOnCreateSectionConfirmButton();
            });
            $el.createSectionModal.on("click", $el.createSectionCancelButtonSelector, function() {
            	$el.createSectionModal.off("click", $el.createSectionConfirmButtonSelector);
            });
        });

        $el.createCopyrightButton.on("click", function () {
        	$el.createCopyrightModal=$($el.createCopyrightModalSelector);
            $el.createCopyrightModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createCopyrightModal.one("click", $el.createCopyrightConfirmButtonSelector, function () {
            	didTapOnCreateCopyrightConfirmButton();
            });
            $el.createCopyrightModal.on("click", $el.createCopyrightCancelButtonSelector, function() {
            	$el.createCopyrightModal.off("click", $el.createCopyrightConfirmButtonSelector);
            });
        });

        $el.createDedicationButton.on("click", function () {
        	$el.createDedicationModal=$($el.createDedicationModalSelector);
            $el.createDedicationModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createDedicationModal.one("click", $el.createDedicationConfirmButtonSelector, function () {
            	didTapOnCreateDedicationConfirmButton();
            });
            $el.createDedicationModal.on("click", $el.createDedicationCancelButtonSelector, function() {
            	$el.createDedicationModal.off("click", $el.createDedicationConfirmButtonSelector);
            });
        });

        $el.createForwardButton.on("click", function () {
        	$el.createForwardModal=$($el.createForwardModalSelector);
            $el.createForwardModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createForwardModal.one("click", $el.createForwardConfirmButtonSelector, function () {
            	didTapOnCreateForwardConfirmButton();
            });
            $el.createForwardModal.on("click", $el.createForwardCancelButtonSelector, function() {
            	$el.createForwardModal.off("click", $el.createForwardConfirmButtonSelector);
            });
        });

        $el.createPrefaceButton.on("click", function () {
        	$el.createPrefaceModal=$($el.createPrefaceModalSelector);
            $el.createPrefaceModal.modal({
                keyboard: false,
                backdrop: true,
                show:true
            });

            $el.createPrefaceModal.one("click", $el.createPrefaceConfirmButtonSelector, function () {
            	didTapOnCreatePrefaceConfirmButton();
            });
            $el.createPrefaceModal.on("click", $el.createPrefaceCancelButtonSelector, function() {
            	$el.createPrefaceModal.off("click", $el.createPrefaceConfirmButtonSelector);
            });
        });
        
	};
	
	// TODO: เธ�เธฑเธ”เธ�เธฒเธฃ refactor code เธชเน�เธงเธ�เธ�เธตเน�เธ�เธญเธ�เธ—เธญเธช
	var onNestableEvents=function() {

		// เธ�เน�เธญเธ�เธ—เธณเธ�เธฃเธฃเธ—เธฑเธ”เธ�เธตเน�เน�เธ”เน� เธ•เน�เธญเธ�เธกเธต element เธ�เธตเน�เน�เธ� DOM เน�เธฅเน�เธง
		$("#nestable").nestable({
			group: 1,
            maxDepth: 2,
            expandBtnHTML: "",
            collapseBtnHTML: "",
        });		
		
		var updateOutput = function(e) {
	        var list   = e.length ? e : jQuery(e.target),
	            output = list.data('output');
	        if(output)
	        	output.val(JSON.stringify(list.nestable('serialize')));//, null, 2));
	    };

        // output initial serialised data
        updateOutput(jQuery('#nestable').data('output', jQuery('#nestable-output')));

		$(".dd").slimScroll({
	        height: '300px',
	        width: '100%',
	        size: '5px',
	        color: '#999999',
	        distance: '0px',
	        start:'.dd'
	    });

        jQuery('#nestable-menu').on('click', function(e)
        {
            var target = jQuery(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                jQuery('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                jQuery('.dd').nestable('collapseAll');
            }
        });

        jQuery(".dd-list").on('click', '.dd-item', function(e){

        	console.log("toc clicked");
            e.stopPropagation();

            enableAllControls(); // just in case, to make sure that a dd-handle exists (in disabled state, there will be dd-disable instead)
            
            // remember what to be loaded after saving the currently open page
            var $previouslySelectedItem=$(".dd-selected-item");
            var $currentlySelectedItem=$(this).find("> .dd-handle");
            
        	// first save previously open page, if there is a page opening, and then loading the target page
            HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

        		// load a target html to ckeditor
                var htmlHref=$currentlySelectedItem.parent().attr("data-html-href");
                if(htmlHref)
                	loadHtmlContent(htmlHref, function() {

                		$previouslySelectedItem.removeClass('dd-selected-item');
                        $currentlySelectedItem.addClass('dd-selected-item');
                		
                	}, function() {
                	}, function() {
                	});

            }, function() {
        		alert("เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เน�เธชเธ”เธ�เน€เธ�เธทเน�เธญเธซเธฒเน�เธ”เน� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เน�เธซเธกเน�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�");
            }, function() {
            	//enableAllControls(); done in "loadHtmlContent"
            });	            	

        });

        jQuery(document).on('click', '#btn-expand-all', function(){
            jQuery('.dd').nestable('expandAll');
        });

        jQuery(document).on('click', '#btn-collapse-all', function(){
            jQuery('.dd').nestable('collapseAll');
        });
        
        jQuery('.dd').on('change', function(e) {

        	console.log("toc reordered");

        	disableAllControls();
        	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {
            	// เน€เธกเธทเน�เธญเธกเธตเธ�เธฒเธฃเน€เธ�เธฅเธตเน�เธขเธ�เน�เธ�เธฅเธ�เธ•เธณเน�เธซเธ�เน�เธ�เน€เธฃเธตเธขเธ�เธฃเน�เธญเธขเน�เธฅเน�เธง update เธ�เน�เธฒเธ•เธณเน�เธซเธ�เน�เธ�เน�เธซเธกเน�เธ�เธฅเธฑเธ�เน€เธ�เน�เธฒเน�เธ�
                $(".dd").attr("data-serialize-toc", JSON.stringify($('#nestable').nestable('serialize'))); // IMPORTANT: เน�เธ•เน�เน€เธซเธกเธทเธญเธ�เธ�เธฐเน�เธกเน�เน�เธ”เน�เน�เธ�เน� data-serialize-toc เน�เธฅเน�เธงเธ�เธฐ เธ�เธทเธญ saveToCloud เธ—เธธเธ�เธ�เธฃเธฑเน�เธ�เธ–เธถเธ�เน�เธกเน�เธ•เธณเน�เธซเธ�เน�เธ�เธ�เธฐเน�เธกเน�เน€เธ�เธฅเธตเน�เธขเธ�        		
        	}, function() {
        		alert("เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เน€เธ�เธฅเธตเน�เธขเธ�เธ•เธณเน�เธซเธ�เน�เธ�เน�เธ”เน� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�")
        	}, function() {
        		enableAllControls();
        	});

        });
	};
    
	var init=function(publicationId) {
		$publicationId = publicationId;
		setup(publicationId);
		onTemplateEvents();
		$el.navBar.show();
	};
	
	return {
		init: function(publicationId) {
			init(publicationId);
		},
		disableAllControls: function() {
			disableAllControls();
		},
		enableAllControls: function() {
			enableAllControls();
		},
	};
	
})(jQuery);