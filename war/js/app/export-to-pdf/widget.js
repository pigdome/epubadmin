var ExportToPdfWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		exportToPdfButton: $("#btnExportToPdf"),

		publicationId: $("#publicationId"),
	};
	
	var gcsRootPath="http://commondatastorage.googleapis.com/ipst-epub/";
	var previewTemplate=Handlebars.templates["preview"];

	var exportToPdf=function() {

    	EpubPackageWidget.disableAllControls();
    	// save currently open page, if there is one
    	HtmlUploadWidget.saveCurrentlyOpenPageToCloud(function() {

        	ExportToPdfEndpoint.getAllHtmlsInSpineOrder($el.publicationId.val(), function(htmlStrings) {

    			combineToSingleHtml(htmlStrings);
    			
    		}, function() {
        		alert("ไม่สามารถนำออกเป็นไฟล์ PDF ได้ กรุณาลองอีกครั้ง")
    		}, function() {
        		EpubPackageWidget.enableAllControls();
    		});

    	}, function() {
    		alert("ไม่สามารถนำออกเป็นไฟล์ PDF ได้ กรุณาลองอีกครั้ง")
    		EpubPackageWidget.enableAllControls();
    	}, function() {
    	});
		
	};
	
	var combineToSingleHtml=function(htmlStrings) {
		
		var singleHtmlContent = _.map(htmlStrings, function(htmlString) {
			return htmlString.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/g, '');
		}).join("<div style='page-break-after: always;'></div>");
						
		var previewWindow=window.open("", "พรีวิว", "");
		var previewWindowHtml=previewTemplate(singleHtmlContent);
		previewWindow.document.writeln(previewWindowHtml);
		previewWindow.document.close();
		previewWindow.print();
	};
	
	var init=function() {
		$el.exportToPdfButton.on("click", function() {
			exportToPdf();
		});
	};
	
	return {
		init: function() {
			init();
		}
	};
	
})(jQuery);