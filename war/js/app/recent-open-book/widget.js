var RecentOpenBookWidget=(function($) {
	
	// binding to UIs and caching elements
	var $el = {
		recentOpenBookButton: $("#btnRecentBook"),
		clearRecentOpenBookButton: $("#clearRecentOpenBookButton"),
		recentBookDropDown: $("#recentBookDropDown"),
	};

	// don't forget to include corresponding compiled templates in .jsp
	var recentOpenBookTemplate = Handlebars.templates["recent-open-book"] ; // use precompiled templates for better performance!

	var max=7;
	var authorId;

	var init=function() {
		if (Modernizr.localstorage) {
			  // window.localStorage is available!
		} else {
			console.log("no native support for local storage :( try a fallback or another third-party solution");
		}
	};
	
	var updateUI=function() {
		// clear old list
		$el.recentBookDropDown.find(".recent-open-book").remove();
		
		// load the current list from localStorage
		$el.recentBookDropDown.prepend(recentOpenBookTemplate(JSON.parse(localStorage[authorId])));
	};
	
	// http://stackoverflow.com/questions/3357553/how-to-store-an-array-in-localstorage
	var add=function(publicationId, title) {
		var arrayString=localStorage[authorId];
		var publications;
		if(arrayString==null || arrayString=="") {
			publications=new Array();
		} else {
			publications=JSON.parse(arrayString);

			// remove the old one if it exists
			var index=indexAt(publications, publicationId);
			if(index!=null) {
				publications.splice(index, 1);
			}
			// if the size exceeds the max, remove the oldest one
			if(publications.length >= max) {
				publications.pop();
			}
		}
		publications.unshift({publicationId: publicationId, title: title});
		localStorage[authorId]=JSON.stringify(publications);
	};
	
	var indexAt=function(publications, publicationId) {
		var targetIndex=null;
		$.each(publications, function(i, publication) {
			if(publication.publicationId==publicationId)
				targetIndex=i;
		});
		return targetIndex;
	};
	
	return {
		init: function(id) {
			authorId=id;

			init();
			
			$el.recentOpenBookButton.click(function() {
				updateUI();
			});
			
			$el.clearRecentOpenBookButton.click(function(e) {
				e.preventDefault();
				
				localStorage[authorId]="";
				
				updateUI();
			});
			
			$el.recentBookDropDown.on("click", ".recent-open-book", function() {
				add(this.id, $(this).text());
			});			
		},
		add: function(publicationId, title) {
			add(publicationId, title);
		}
	};
	
})(jQuery);