var ImportFromEpubWidget=(function($) {
	
	var gcsBaseUrl="http://commondatastorage.googleapis.com/ipst-epub/";
	
	// binding to UIs and caching elements
	var $el = {
		importButton: $("input.bootstrap-filestyle"),
		file: $("#importFromEpubFile"),
				
		formControl: $("#importFromEpubFormControl"),
		form: $("#importFromEpubForm"),
		formKey: $("#importFromEpubFormKey"),
		formPolicy: $("#importFromEpubFormPolicy"),
		formSignature: $("#importFromEpubFormSignature"),
		
		spinner: $("#spinner"),
		
		authorId: $("#authorId"),
	};

	var showEpubImportButton=function() {
		// first hide upload form wait until signature and policy fields are updated
		$el.formControl.hide();
		
		// show upload button when form fields are ready
		ImportFromEpubEndpoint.renewFormSignatureAndPolicy(function(data) {
			// update fields
			$el.formPolicy.val(data.policy);
			$el.formSignature.val(data.signature);
						
			setupEpubUploadForm();
			
			// show the upload button
			$el.formControl.show();

		}, function() {
			console.log("failed in ImportFromEpubEndpoint.renewFormSignatureAndPolicy");
		});
	};
	
	var extractEpubForUrl=function(epubUrl) {
		//console.log(epubUrl);
		
		var gcsKey=epubUrl.split(gcsBaseUrl)[1];
		
		ImportFromEpubEndpoint.extractEpub(gcsKey, function(publicationAndEpubPackage) {
			//console.log(publicationAndEpubPackage);
			window.location.href="/"; // refresh the page
		}, function() {
			console.log("failed");			
			alert("เธ�เธณเน€เธ�เน�เธฒเน�เธ�เธฅเน� ePub เน�เธกเน�เธชเธณเน€เธฃเน�เธ� เธ�เธฃเธธเธ“เธฒเธฅเธญเธ�เธญเธตเธ�เธ�เธฃเธฑเน�เธ�");
		}, function() {
        	$el.spinner.css("display", "none");
		})
	};
	
	var setupEpubUploadForm=function() {
		$el.form.fileupload({
			done: function (e, data) {
	            var $xmlResult = $(data.result); // data.result is already an XML document so no need to use $.parseXML()
	            var $location=$xmlResult.find("Location");
	            
	            var epubUrl=$location.text();
	            extractEpubForUrl(epubUrl);
	        },
	        change: function (e, data) {
	        	$el.spinner.css("display", "inline-block");
	        }, 
	        add: function(e, data) {
	        	//console.log(data);
	        	
                var uploadErrors = [];
                var fileName = data.originalFiles[0]['name'].toString();
                var fileTypes = fileName.split("."); // type เธ�เธทเธญ mimetype เน�เธกเน�เน�เธ�เน�เน�เธ�เน� file extension      
                if((fileTypes[1].localeCompare("epub"))!=0) {
				
                    uploadErrors.push('เธญเธ�เธธเธ�เธฒเธ•เน�เธซเน�เธ�เธณเน€เธ�เน�เธฒเน€เธ�เธ�เธฒเธฐเน�เธ�เธฅเน� ePub เน€เธ—เน�เธฒเธ�เธฑเน�เธ�');
                }
                if(data.originalFiles[0]['size'] > 150000000) { // 150 MB
                    uploadErrors.push('เน�เธ�เธฅเน�เธกเธตเธ�เธ�เธฒเธ”เน�เธซเธ�เน�เน€เธ�เธดเธ�เน�เธ� เน�เธกเน�เธชเธฒเธกเธฒเธฃเธ–เธ�เธณเน€เธ�เน�เธฒเน�เธ”เน�');
                }
                if(uploadErrors.length > 0) {
                	$el.spinner.css("display", "none");
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
	        },
		});		

		// set GCS key of pdfForm
		var key=$el.authorId.val()+"/temp-imported-epub/${filename}"; 
		$el.formKey.val(key);
	};
	
	var adjustImportButtonPosition=function() {
		// workaround to align positions with other buttons
		$("label[for='importFromEpubFile']").attr("style", "margin-top:10px; margin-right:10px; width:75px;");
	};

	
	return {
		init: function() {
			
			/* this simple hack of a button triggering on input's file click doesn't work on the second time, use bootstrap filestyle instead (http://markusslima.github.io/bootstrap-filestyle/)
			$el.importButton.click(function() {
				$el.file.trigger("click");
			});
			*/

			adjustImportButtonPosition();			
			showEpubImportButton();
		}
	};
	
})(jQuery);