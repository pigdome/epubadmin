var PublicationModel=(function($) {

	// binding to input UIs and caching elements
	var $el = {
		authorId: $("#authorId"),
		otherTitles: $("#otherTitles"),
		otherTitle: $("#otherTitle"),
		authorsTypes_authors: $("#authorsTypes_authors"),
		authorType: $("#authorType"),
		author: $("#author"),
		subjectHeadings: $("#subjectHeadings"),
		subjectHeading: $("#subjectHeading"),
	    title: $(".input-book-title"),
	    mediaTypes: $("#mediaTypes"),
	    resourceTypes: $("#resourceTypes"),
	    disciplines: $("#disciplines"),
	    grades: $("#grades"),
	    description: $("#description"),
		selectedCoverImg: $("#preview-book-cover-create-new img"), 
		themeId: $("#themeId"),
	};
	
	var authorsTypesDictionary = {
		"author" : "ผู้แต่ง",
		"editor" : "ผู้ปรับปรุง/ผู้แก้ไข/บรรณาธิการ",
		"scriptwriter" : "ผู้เขียนบท",
		"instructionaldesigner" : "ผู้ออกแบบการสอน",
		"graphicdesigner" : "ผู้ออกแบบกราฟิก",
		"subjectmatterexpert" : "ผู้เชี่ยวชาญเฉพาะสาขา",
		"other" : "ผู้ร่วมงานอื่นๆ"
	};
	
	var getPublicationFromInputs=function() {
		return {
			authorId: $el.authorId.val().trim(),					
			coverUrl: $el.selectedCoverImg.data("url"),				
			title: $el.title.val().trim(),					
			otherTitles: $el.otherTitles.find("option").map(function () {
				return this.value.trim();
			}).get(), 
			authors: $el.authorsTypes_authors.find("option").map(function () {
				return this.value.trim().split(":")[1];
			}).get(),
			authorsTypes: $el.authorsTypes_authors.find("option").map(function () {
				return this.value.trim().split(":")[0];
			}).get(),
			mediaTypes: $el.mediaTypes.find("option:selected").map(function() {
				return this.value.trim();
			}).get(),
			resourceTypes: $el.resourceTypes.find("option:selected").map(function() {
				return this.value.trim();
			}).get(),
			disciplines: $el.disciplines.map(function() {
				return this.value.trim();
			}).get(),
			grades: $el.grades.map(function() {
				return this.value.trim();
			}).get(),
			description: { value: $el.description.val().trim() },
			subjectHeadings: $el.subjectHeadings.find("option").map(function() { 
				return this.value.trim(); 
			}).get(),
			themeId: $el.themeId.val().trim(),
		}		
	};
	
	return {
		getPublicationWithIdFromInputs: function() {
			var publicationFromInputs=getPublicationFromInputs();
			var originalPublication=$(".book-item-now-selected img").data("book");
			var modifiedPublication=$.extend({}, originalPublication, publicationFromInputs);
			return modifiedPublication;
		},
		getPublicationFromInputs: getPublicationFromInputs,
		setPublicationToInputs: function(publication) {
			$el.title.val(publication.title);
			if(publication.otherTitles) {
				$.each(publication.otherTitles, function(i, value) {
					$el.otherTitles
						.append($("<option></option>")
								.attr("value", value)
								.text(value));
				});
			}
			if(publication.authors) {
				$.each(publication.authors, function(i, value) {
					$el.authorsTypes_authors
						.append($("<option></option>")
								.attr("value", publication.authorsTypes[i]+":"+ value)
								.text(authorsTypesDictionary[publication.authorsTypes[i]]+":"+ value));
				});
			}

			$el.mediaTypes.val(publication.mediaTypes);
			$el.resourceTypes.val(publication.resourceTypes);
			$el.disciplines.val(publication.disciplines);
			$el.grades.val(publication.grades);
			if(publication.description)
				$el.description.val(publication.description.value);
			else 
				$el.description.val("");
			if(publication.subjectHeadings) {
				$.each(publication.subjectHeadings, function(i, value) {
					$el.subjectHeadings
						.append($("<option></option>")
								.attr("value", value)
								.text(value));
				});
			}
			
			$el.themeId.val(publication.themeId);

			/* ผิดแล้ว code ตรงนี้ใช้ตอน save แต่นี่คือตอน load
			var $smallCover=$("#"+book.id);
			var $smallCoverImg=$smallCover.find("> img");
			$smallCoverImg.data("url", book.coverUrl);
			loadImageAsync($smallCoverImg);
			
			var $bigCover=$smallCover.find(".og-fullimg");
			var $bigCoverImg=$bigCover.find("> img");
			$bigCoverImg.data("url", book.coverUrl);
			loadImageAsync($bigCoverImg);
			*/
			$el.selectedCoverImg.data("url", publication.coverUrl);
			PublicationEndpoint.loadImageAsync($el.selectedCoverImg);		
		},
		getPublicationByElementId: function(elementId) {
			return $("#"+elementId).data("book");
		},
		clearPublicationInputs: function() {
			$el.title.val("");
			$el.otherTitle.val("");
			$el.otherTitles.html("");
			$el.author.val("");
			$el.authorsTypes_authors.html("");
			$el.mediaTypes.val("");
			$el.resourceTypes.val("");
			$el.disciplines.val("");
			$el.grades.val("");
			$el.description.val("");
			$el.subjectHeading.val("");
			$el.subjectHeadings.html("");
			$el.selectedCoverImg.attr("src", "/assets/img/book-covers/book-cover-preview-new-book.png");
			$el.themeId.val("");
		},
		addOtherTitle: function() {
			$el.otherTitles
				.append($("<option></option>")
					.attr("value", $el.otherTitle.val().trim())
					.text($el.otherTitle.val().trim()));
			$el.otherTitle.val("");
		},
		removeSelectedOtherTitle: function() {
			$el.otherTitles.find("option:selected").remove();
		},
		addAuthor: function() {
			$el.authorsTypes_authors
				.append($("<option></option>")
					.attr("value", $el.authorType.val() + ":" + $el.author.val().trim())
					.text($el.authorType.find("option:selected").text()+":"+$el.author.val().trim()));
			$el.author.val("");
		},
		removeSelectedAuthor: function() {
			$el.authorsTypes_authors.find("option:selected").remove();
		},
		addSubjectHeading: function() {
			$el.subjectHeadings
				.append($("<option></option>")
					.attr("value", $el.subjectHeading.val().trim())
					.text($el.subjectHeading.val().trim()));
			$el.subjectHeading.val("");
		},
		removeSelectedSubjectHeading: function() {
			$el.subjectHeadings.find("option:selected").remove();
		}
	};
	
})(jQuery);
