<!DOCTYPE html> 
<%@page import="th.ac.ipst.epub.oauth.UserValidator"%>
<%@page contentType="text/html;charset=UTF-8" %>
<%
	UserValidator validator=new UserValidator();
%>
<html lang="en">

  	<!--% if(validator.validate(request.getSession())) { 
			Long authorId=(Long)request.getSession().getAttribute("authorId");
			String authorDisplayName=(String)request.getSession().getAttribute("authorDisplayName");
			Boolean newMember=(Boolean)request.getSession().getAttribute("newMember");
	%-->
  	<% if(true) { 
			Long authorId = Long.parseLong("5742796208078848");
			String authorDisplayName="apirak pigdome";
			Boolean newMember=false;
	%>
	
  <head>
  
    <meta charset="utf-8">
    <title>IPST ePub Editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<link rel="stylesheet" href="/fonts/thsarabunnew.css" />

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/component.css" rel="stylesheet">
    <link href="assets/css/og-responsive.css" rel="stylesheet">
   

    <style type="text/css">

      body {
        /* font-family: 'THSarabunNew', sans-serif; */
        padding-top: 60px;
        padding-bottom: 40px;        
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      .panel{
      min-width: 100%;
      height: 100%;
      top:-120%;
      left:0;
      overflow-y: auto;
      overflow-x: hidden;
      position: absolute;
      background: lightgray;
      box-shadow: 0px 4px 7px rgba(0,0,0,0.6);
      z-index: 9999;

      }
      .book-icon{
        cursor: pointer;
        width: 90px;
        height: 117px;
        margin : 5px 5px 5px 0;
        box-shadow: 1px 1px 2px 1px #BABABA;
      }

      #bookCoverL{

        box-shadow: 1px 1px 2px 1px #BABABA;
      }
      .bookCoverL {
		width: 300px;
		height: 391px;
	  }
      
      .modal{
        width:900px;
        height:550px;

      }
      .modal-body{
        height:450px;
        max-height: 550px;

      }

      .book-cover-selected{
        box-shadow : 0px 0px 0px 6px darkgray;
      }
      .icon-trash-book-cover{
        position:absolute;
        right:0;
        margin-top: 22px;
        margin-right: 6px;
        cursor: pointer;

      }
      .form-btn-delete-my-cover{
        position:absolute;
        right:0;
        bottom: 0;
        margin-right: 5px;
        margin-bottom:13px;
        display: none;

      }

      #item-cover-templates img{
        margin-top:20px;
        margin-bottom:5px;
        margin-left:15px;
        height:100px;
        cursor: pointer;
        /*box-shadow: 1px 1px 2px 1px #BABABA;*/

      }

      #item-my-covers img{
        margin-top:20px;
        margin-bottom:5px;
        margin-left:15px;
        height:100px;
        cursor: pointer;
        /*box-shadow: 1px 1px 2px 1px #BABABA;*/

      }

      #preview-book-cover-create-new img{
        width:130px;
        box-shadow: 1px 1px 2px 1px #BABABA;

      }

/*      .book-item{
        transition: height 350ms ease;
        -webkit-transition: height 350ms ease;
      }*/

            *,
      *:after,
      *:before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 0;
        margin: 0;
      }

      .form-horizontal .control-label{
        width:120px;
      }
      .form-horizontal .controls{
        margin-left: 140px;
      }


      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }

      }

      @media (min-width: 1200px) {
      
        .modal{
          width:1000px;
          margin-left:-500px;

        }



      }

      @media (max-width: 1200px) {
      
        .modal{
          width:900px;
           margin-left:-450px;
        }

        #txt-search-book-item{
          width:250px;
        }
      }

      @media (max-width: 979px) {
      
        .modal{
          width:760px;
          margin-left: -380px;
        }
        .input-book-title{
          width:160px;
        }
        #txt-search-book-item{
          width:300px;
        }
      }

      @media (max-width: 767px) {
      
        .modal{
          width:auto;
          margin-left: 10px;
        }
                #txt-search-book-item{
          width:300px;
        }
      }

      @media (max-width: 480px) {
      
        .modal{
          width:auto;

        }
                #txt-search-book-item{
          width:300px;
        }
      }

    </style>
 <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>

    <div class="navbar navbar-fixed-top" style="display:none;">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">IPST ePub Editor</a>
          <div class="nav-collapse collapse">
<!--             <p id="signin"class="navbar-text pull-right" style="margin-top:5px;">
              Sign in with  
              <img id="imgFacebookIcon" src='assets/img/idp-logos/facebookIcon.png' alt='facebook' style="margin-left:10px; cursor:pointer;"/> 
              <img id="imgTwitterIcon" src='assets/img/idp-logos/twitterIcon.png' alt='twitter' style="cursor:pointer;"/> 
              <img id="imgGoogleIcon" src='assets/img/idp-logos/googleIcon.png' alt='google' style="cursor:pointer;"/> 
            </p> -->
            <ul class="nav">
              <li>
                    <div class="btn-group">
                    <button id="btnRecentBook" class="btn btn-mini dropdown-toggle" data-toggle="dropdown" style="margin-top:5px; margin-right:12px; width:110px;"><i class="icon-book icon-black"></i> เปิดล่าสุด<span class='caret'></span></button>
                    <ul id="recentBookDropDown" class="dropdown-menu">
                        <li class="divider" style="border:whitesmoke 1px solid;"></li>
                        <li id="clearRecentOpenBookButton"><a href="#"><i class="icon-remove-circle"></i> ล้างค่าทั้งหมด</a></li>
                    </ul>
                  </div>
              </li>  

              <li>
                <button id="btnNewBook" class="btn btn-mini" type="button" style="margin-top:10px; margin-right:10px; width:70px;">
                  <i class="icon-file icon-black"></i> สร้าง
                </button>
              </li>

              <li id="importFromEpubFormControl">
            	<form id="importFromEpubForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="key" id="importFromEpubFormKey" value="" />
					<input type="hidden" name="bucket" value="ipst-epub" />
					<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com" />
					<input type="hidden" name="policy" id="importFromEpubFormPolicy" />
					<input type="hidden" name="signature" id="importFromEpubFormSignature" />
					<input type="hidden" name="success_action_status" value="201" />
					
					<input id="importFromEpubFile" name='file' type="file" class="filestyle" data-classIcon="icon-share-alt icon-black" data-classButton="btn btn-mini" data-buttonText="นำเข้า" data-input="false"  />

				</form>
              </li>
              
              <li>
              	<div id="spinner" style="display: none;"><img class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 11px;" /></div>
              </li>
              
              <!--  
              <li>
                  <div class="input-append" Style="margin-bottom:10px; margin-top:10px;">
                    <input class="span5" id="txt-search-book-item" type="text" style="height:30px;">
                    <button id="btnSearchBook" class="btn" type="button"><i class="icon-search icon-black"></i></button>
                  </div>
              </li>
              -->
            </ul>

            <ul class="nav nav-pills pull-right" style="margin-top: 0px;">
            <li> 
            <div class="btn-group">
                    <button id="loginButton" class="btn dropdown-toggle" data-toggle="dropdown"><%= authorDisplayName %> <span class='caret'></span></button>
                    <ul class="dropdown-menu">
                    <!--  
                        <li><a href="#"><i class="icon-user"></i> Profile</a></li>
					-->                        
                        <li class="divider" style="border:whitesmoke 1px solid;"></li>
                        <li id="logoutButton"><a href="#"><i class="icon-off"></i> Logout</a></li>
                    </ul>
                  </div>
            </li>          
          </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div id="epub-container" class="container-fluid" style="display:none;">
      <div class="row-fluid">
          <div class="span12">
              <div class="main">
                  <ul id="og-grid" class="og-grid">

                  </ul> 
              </div> 
          </div>
      </div>
    </div><!--/.fluid-container-->


    <!--  
    <div id="detailBookPanel" class="panel">
      <img src='assets/img/book-covers/bookCoverBig.png' alt='img01'/>
    </div>
    -->

    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel"></h3>
            </div>
            <div class="modal-body">
                  <div class="container-fluid">
                  <div class="row-fluid">
                  
                    <div class="span3">
                      <!--Sidebar content-->
                        <div id="preview-book-cover-create-new" style="text-align:center;">
                            <img src='assets/img/book-covers/book-cover-0.png'/>
                        </div>
                    </div>
                    <div class="span9">
                      <!--Body content-->
                        <ul id="myTab" class="nav nav-tabs">
                          <li><a href="#tab-book-cover" data-toggle="tab">หน้าปก</a></li>
                          <li><a href="#tab-titles" data-toggle="tab">ชื่อเรื่อง</a></li>
                          <li><a href="#tab-authors" data-toggle="tab">ผู้แต่ง</a></li>
                          <li><a href="#tab-description" data-toggle="tab">คำอธิบายหนังสือ</a></li>
                          <li><a href="#tab-metadata" data-toggle="tab">หมวดหมู่</a></li>
                          <li><a href="#tab-book-theme" data-toggle="tab">รูปแบบหนังสือ</a></li>
                        </ul>

                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade" id="tab-book-cover">

                                <ul class="nav nav-pills">
                                    <li id="nav-cover-templates"class="active"><a href="#">แม่แบบ</a></li>
                                    <li id="nav-my-cover"><a href="#">ทำเอง</a></li>
                                </ul>

                                <div id="item-cover-templates" style="overflow:auto; height:100%;">
                                </div>  


                                <div id="item-my-covers" style="overflow:auto; height:100%; display:none;">
	
							      	<div id="myCoverUploadControl">
										<form id="myCoverForm" action="http://ipst-epub.storage.googleapis.com/" method="POST" enctype="multipart/form-data">
											<input type="hidden" name="key" id="myCoverKey" value="" >
											<input type="hidden" name="bucket" value="ipst-epub">
											<input type="hidden" name="GoogleAccessId" value="521169348394-fldq88uf0hui6vd2u2cdb2hu18as9ba7@developer.gserviceaccount.com">
											<input type="hidden" name="policy" id="myCoverPolicy" >
											<input type="hidden" name="signature" id="myCoverSignature" >
											<input type="hidden" name="success_action_status" id="success_action_status" value="201">
										  	<!-- The fileinput-button span is used to style the file input field as button -->
											<div>										  	
										  		<span id="myCoverUploadButton" class='btn btn-success fileinput-button'>
										    		<i class='icon-plus icon-white'></i>
										    		<span>Upload</span>
										    		<input name='file' type='file'>
										  		</span>
												<span id="myCoverFormSpinner" style="display: none;"><img class="spinner" src="/img/loading51.gif" style="width: 20px; height: 20px; margin-left: 5px; margin-top: 2px;" /></span>
											</div>
										</form>					      		
							      	</div>
							      	
                                </div>  

                            </div>
                            
                            
                           	<div class="tab-pane fade" id="tab-titles">
                               <div class="meta-data-div">
                                <form class="form-horizontal">
                                  <div class="control-group">
                                    <label class="control-label">ชื่อเรื่อง</label>
                                    <div class="controls">
                                       <input class="input-book-title"type="text" placeholder="" style="width:260px; height:30px;">
                                    </div>
                                  </div>
                                  
                                  <div class="control-group">
                                    <label class="control-label">ชื่อเรื่องอื่นๆ</label>
                                    <div class="controls">
                                      <div class="input-append">
                                        <input id="otherTitle" type="text" style="width:260px; height:30px;">
                                        <button id="addOtherTitleButton" class="btn" type="button"><i class="icon-plus icon-black"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <select id="otherTitles" multiple="multiple" style="width:300px; height:70px; margin-top:-13px;">
                                      </select>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <button id="removeOtherTitleButton" class="btn btn-small" type="button" style="width: 300px;margin-top: -25px;">ลบรายการที่เลือก</button>
                                    </div>
                                  </div> 
                                </form>

                              </div>
                            </div>
                            
                            
                           	<div class="tab-pane fade" id="tab-authors">
                               <div class="meta-data-div">
                                <form class="form-horizontal">
                                  <div class="control-group">
                                    <label class="control-label">ผู้แต่ง / ผู้ร่วมงาน</label>
                                    <div class="controls">
                                      <div class="input-append">
                                        <input id="author" type="text" style="width:260px; height:30px;">
                                        <button class="btn" type="button" id="addAuthorButton"><i class="icon-plus icon-black"></i></button>
                                      </div>                    

                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <select id="authorType" style="width: 300px; margin-top:-20px;"> <!-- value ข้างล่างเก็บตาม LOM -->
                                        <option value="author">ผู้แต่ง</option>
                                        <option value="editor">ผู้ปรับปรุง/ผู้แก้ไข/บรรณาธิการ</option>
                                        <option value="scriptwriter">ผู้เขียนบท</option>
                                        <option value="instructionaldesigner">ผู้ออกแบบการสอน</option>
                                        <option value="graphicdesigner">ผู้ออกแบบกราฟิก</option>
                                        <option value="subjectmatterexpert">ผู้เชี่ยวชาญเฉพาะสาขา</option>
                                        <option value="other">ผู้ร่วมงานอื่นๆ</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <select id="authorsTypes_authors" multiple="multiple" style="width:300px; height:70px; margin-top:-17px;">
                                      </select>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <button id="removeAuthorButton" class="btn btn-small" type="button" style="width: 300px;margin-top: -25px;">ลบรายการที่เลือก</button>
                                    </div>
                                  </div>
                                </form>

                              </div>
                            </div>   
                            
                                                  
                            <div class="tab-pane fade" id="tab-description">
                               <div class="meta-data-div">


                                <form class="form-horizontal">
                                  <div class="control-group">
                                    <label class="control-label">รายละเอียด</label>
                                    <div class="controls">
                                      <textarea rows="6" id="description" style="width:260px;"></textarea>

                                    </div>
                                  </div>
                                  
                                  <div class="control-group">
                                    <label class="control-label">หัวเรื่อง</label>
                                    <div class="controls">
                                      <div class="input-append">
                                        <input id="subjectHeading" type="text" style="width:260px; height:30px;">
                                        <button id="addSubjectHeadingButton" class="btn" type="button"><i class="icon-plus icon-black"></i></button>
                                      </div>
                                    </div>

                                  </div>

                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <select id="subjectHeadings" multiple="multiple" style="width:300px; height:70px; margin-top:-13px;">
                                      </select>
                                    </div>

                                  </div>

                                  <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                      <button id="removeSubjectHeadingButton" class="btn btn-small" type="button" style="width: 300px;margin-top: -25px;">ลบรายการที่เลือก</button>
                                    </div>
                                  </div>
                                                                    
                                </form>
                              </div>
                            </div>


                            <div class="tab-pane fade" id="tab-metadata">
                              <div class="meta-data-div">


                                <input type="hidden" id="itemId" >
                                <input type="hidden" id="downloadUrl" value="N/A">
                                <input type="hidden" id="dateAdded" >
                                <input type="hidden" id="downloadCount" >
                                <input type="hidden" id="thumbnailUrl" value="N/A">
                                <input type="hidden" id="preview1Url" value="N/A">
                                <input type="hidden" id="preview2Url" value="N/A">
                                <input type="hidden" id="preview3Url" value="N/A">

                                <form class="form-horizontal">

               


                                  <div class="control-group">
                                    <label class="control-label">ชนิดของสื่อ</label>
                                    <div class="controls">
                                      <select id="mediaTypes" multiple="multiple" style="width:300px; height:100px; "> <!-- value ข้างล่างเก็บตาม http://dublincore.org/documents/2006/08/28/dcmi-type-vocabulary/ -->
                                        <option value="Video">Video</option>
                                        <option value="Sound">Sound</option>
                                        <option value="Text">Text</option>
                                        <option value="Image">Image</option>
                                        <option value="InteractiveResource">Interactive Resource</option>
                                        <option value="Software">Software</option>
                                        <option value="PhysicalObject">Physical Object</option>
                                        <option value="Dataset">Dataset</option>
                                      </select>
                                      <div style="font-size:12px; color:gray; margin-left:5px;"><sup>*</sup> (สามารถระบุได้มากกว่าหนึ่งค่า)</div>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">ประเภทสื่อการเรียนรู้</label>
                                    <div class="controls">
                                      <select id="resourceTypes" multiple="multiple" style="width:300px; height:100px; ">
                                        <option value="exercise">Exercise</option>
                                        <option value="simulation">Simulation</option>
                                        <option value="demonstration">Demonstration</option>
                                        <option value="experiment">Experiment</option>
                                        <option value="exam">Exam</option>
                                        <option value="game">Game</option>
                                        <option value="narrative text">Narrative Text</option>
                                        <option value="questionaire">Questionaire</option>
                                        <option value="diagram">Diagram</option>
                                        <option value="figure">Figure</option>
                                        <option value="graph">Graph</option>
                                        <option value="index">Index</option>
                                        <option value="slide">Slide</option>
                                        <option value="table">Table</option>
                                        <option value="problem statement">Problem Statement</option>
                                        <option value="self assessment">Self Assessment</option>
                                        <option value="lecture">Lecture</option>
                                      </select>
                                      <div style="font-size:12px; color:gray; margin-left:5px;"><sup>*</sup> (สามารถระบุได้มากกว่าหนึ่งค่า)</div>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">กลุ่มสาระวิชา</label>
                                    <div class="controls">
                                      <select id="disciplines" multiple="multiple" style="width:300px; height:100px; ">
                                        <option value="คณิตศาสตร์">คณิตศาสตร์</option>
                                        <option value="การออกแบบและเทคโนโลยี">เทคโนโลยี: การออกแบบและเทคโนโลยี</option>
                                        <option value="คอมพิวเตอร์และไอซีที">เทคโนโลยี: คอมพิวเตอร์และไอซีที</option>
                                        <option value="วิทยาศาสตร์ (ประถม-มัธยม)">วิทยาศาสตร์: ประถม-มัธยม</option>
                                        <option value="เคมี">วิทยาศาสตร์: เคมี</option>
                                        <option value="ชีววิทยา">วิทยาศาสตร์: ชีววิทยา</option>
                                        <option value="ฟิสิกส์">วิทยาศาสตร์: ฟิสิกส์</option>
                                        <option value="โลกทั้งระบบ">วิทยาศาสตร์: โลกทั้งระบบ</option>
                                        <option value="โลก ดาราศาสตร์และอวกาศ">วิทยาศาสตร์: โลก ดาราศาสตร์และอวกาศ</option>
                                      </select>
                                      <div style="font-size:12px; color:gray; margin-left:5px;"><sup>*</sup> (สามารถระบุได้มากกว่าหนึ่งค่า)</div>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">ระดับชั้นเรียน</label>
                                    <div class="controls">
                                      <select id="grades" multiple="multiple" style="width:300px; height:100px; ">
                                        <option value="ปฐมวัย">ปฐมวัย</option>
                                        <option value="ประถมศึกษาปีที่ 1">ประถมศึกษาปีที่ 1</option>
                                        <option value="ประถมศึกษาปีที่ 2">ประถมศึกษาปีที่ 2</option>
                                        <option value="ประถมศึกษาปีที่ 3">ประถมศึกษาปีที่ 3</option>
                                        <option value="ประถมศึกษาปีที่ 4">ประถมศึกษาปีที่ 4</option>
                                        <option value="ประถมศึกษาปีที่ 5">ประถมศึกษาปีที่ 5</option>
                                        <option value="ประถมศึกษาปีที่ 6">ประถมศึกษาปีที่ 6</option>
                                        <option value="มัธยมศึกษาปีที่ 1">มัธยมศึกษาปีที่ 1</option>
                                        <option value="มัธยมศึกษาปีที่ 2">มัธยมศึกษาปีที่ 2</option>
                                        <option value="มัธยมศึกษาปีที่ 3">มัธยมศึกษาปีที่ 3</option>
                                        <option value="มัธยมศึกษาปีที่ 4">มัธยมศึกษาปีที่ 4</option>
                                        <option value="มัธยมศึกษาปีที่ 5">มัธยมศึกษาปีที่ 5</option>
                                        <option value="มัธยมศึกษาปีที่ 6">มัธยมศึกษาปีที่ 6</option>
                                        <option value="อุดมศึกษา">อุดมศึกษา</option>
                                      </select>
                                      <div style="font-size:12px; color:gray; margin-left:5px;"><sup>*</sup> (สามารถระบุได้มากกว่าหนึ่งค่า)</div>
                                    </div>
                                  </div>


                                </form>


                              </div>  
                            </div>

                            <div class="tab-pane fade" id="tab-book-theme">
                               <div class="book-theme-div" style="height:100%;">
                                
                                <ul class="nav nav-pills">
                                  <li><div style="margin-top:7px; margin-right:10px;">รูปแบบหนังสือ : </div></li>
                                  <li class="dropdown">
                                    <a class="dropdown-toggle" id="selectedTheme" role="button" data-toggle="dropdown" href="#">เรียบง่าย <b class="caret"></b></a>
                                    <ul id="theme-menu" class="dropdown-menu" role="menu" aria-labelledby="drop4" style="height:320px; overflow:auto; width:200px;">

                                      <li id="defaultTheme" class="book-theme-selection theme-selected" role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#"> 
                                          <img style="height:60px; padding:5px; margin-right:15px;" src='assets/img/template/chapter/theme-1/template-1.png' class="img-polaroid"/> เรียบง่าย
                                        </a>
                                      </li>

                                      <li class="book-theme-selection" role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#">
                                          <img style="height:60px; padding:5px; margin-right:15px;" src='assets/img/template/chapter/theme-2/template-1.png' class="img-polaroid"/> เรียบง่าย 2
                                        </a>
                                      </li>

                                      <li class="book-theme-selection" role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#">
                                        <img style="height:60px; padding:5px; margin-right:15px;" src='assets/img/template/chapter/theme-3/template-2.png' class="img-polaroid"/> คลาสสิค
                                        </a>
                                      </li>

                                    </ul>
                                  </li>
                                </ul>


                                <div id="preview-theme-1" class="well preview-theme" style="padding:25px; background-color:whitesmoke; display:none;">
                                  <div style="margin-bottom:20px;"><span class="badge badge-info">บท</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-1/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-1/template-2.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-1/template-3.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-1/template-4.png' class="img-polaroid"/>
                                  <br>
                                  <div style="margin-bottom:20px;"><span class="badge badge-success">หัวข้อ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-1/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-1/template-2.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-1/template-3.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-1/template-4.png' class="img-polaroid"/>

                                  <div style="margin-bottom:20px;"><span class="badge badge-important">อื่นๆ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/copyright/theme-1/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/dedication/theme-1/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/forward/theme-1/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/preface/theme-1/template-1.png' class="img-polaroid"/>

                                                                    
                                </div>  


                                <div id="preview-theme-2" class="well preview-theme" style="padding:25px; background-color:whitesmoke; display:none;">
                                  <div style="margin-bottom:20px;"><span class="badge badge-info">บท</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-2/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-2/template-2.png' class="img-polaroid"/>
                                  <br>
                                  <div style="margin-bottom:20px;"><span class="badge badge-success">หัวข้อ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-2/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-2/template-2.png' class="img-polaroid"/>


                                  <div style="margin-bottom:20px;"><span class="badge badge-important">อื่นๆ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/copyright/theme-2/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/dedication/theme-2/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/forward/theme-2/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/preface/theme-2/template-1.png' class="img-polaroid"/>

                                                                    
                                </div>  

                                <div id="preview-theme-3" class="well preview-theme" style="padding:25px; background-color:whitesmoke; display:none;">
                                  <div style="margin-bottom:20px;"><span class="badge badge-info">บท</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-3/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/chapter/theme-3/template-2.png' class="img-polaroid"/>
                                  <br>
                                  <div style="margin-bottom:20px;"><span class="badge badge-success">หัวข้อ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-3/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/section/theme-3/template-2.png' class="img-polaroid"/>


                                  <div style="margin-bottom:20px;"><span class="badge badge-important">อื่นๆ</span> </div>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/copyright/theme-3/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/dedication/theme-3/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/forward/theme-3/template-1.png' class="img-polaroid"/>
                                  <img style="height:140px; padding:5px; margin-right:15px; margin-bottom:30px;" src='assets/img/template/preface/theme-3/template-1.png' class="img-polaroid"/>

                                                                    
                                </div>  

                              </div>
                            </div>


                        </div>
                    </div>
                  </div>
                </div>

            </div>
            <div class="modal-footer">
              <button class="btn" data-dismiss="modal">ยกเลิก</button>
              <button id="btnSaveNewBook" class="btn btn-primary">บันทึก</button>
            </div>
    </div>





		<input type="hidden" id="authorId" value="<%= authorId %>">
  		<input type="hidden" id="authorDisplayName" value="<%= authorDisplayName %>">
  		<input type="hidden" id="newMember" value="<%= newMember %>">
  		<input type="hidden" id="themeId" >

    <!-- Placed at the end of the document so the pages load faster -->

  		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

		<script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap-filestyle.min.js"> </script>
		<script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
		<script type="text/javascript" src="assets/js/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/modernizr/modernizr.custom.59005.js"></script>
		<script src="/js/underscore/underscore-min.js"></script>
		
		<link rel='stylesheet' href='/css/jquery-file-upload/jquery.fileupload-ui.css'>
		<script src='/js/jquery-file-upload/vendor/jquery.ui.widget.js'></script>
		<script src='/js/jquery-file-upload/jquery.fileupload.js'></script>
		<link rel='stylesheet' href='/css/my-cover-progress-bar.css'>		
		
		<script src="/js/xdate.js"></script>
		<script src="/js/imagesloaded.pkgd.min.js"></script>
		<script src="/js/imagesloaded.js"></script>
		<script src="/js/utils.js"></script>
		
		<script src="/js/handlebars/handlebars.runtime.js"></script>
		<script src="/js/handlebars-helpers.js"></script>
		<script src="/tmpl/cover.js"></script>
		<script src="/tmpl/my-cover.js"></script>
		<script src="/tmpl/book.js"></script>
		<script src="/tmpl/book-details.js"></script>
		<script src="/tmpl/book-details-reuse.js"></script>
		<script src="/tmpl/recent-open-book.js"></script>

<!--  
    <script src="assets/js/epubeditor.js"></script>
-->

		<script src="/js/app/author/endpoint.js"></script>
		<script src="/js/app/book-cover/endpoint.js"></script>
		<script src="/js/app/publication/widget.js"></script>
		<script src="/js/app/publication/endpoint.js"></script>
		<script src="/js/app/publication/model.js"></script>
		<script src="/js/app/recent-open-book/widget.js"></script>
		<script src="/js/app/suggested-tag/endpoint.js"></script>
		<script src="/js/app/delete-publication/widget.js"></script>	
		<script src="/js/app/import-from-epub/widget.js"></script>	
		<script src="/js/app/import-from-epub/endpoint.js"></script>	

		<script>
			(function() {
				PublicationWidget.init();
				RecentOpenBookWidget.init($("#authorId").val());
				DeletePublicationWidget.init();
				ImportFromEpubWidget.init();
			})();
			/*
			$(document).ready(function() {
				PublicationWidget.init();				
			});
			*/
		</script>

		<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		 ga('create', 'UA-50962149-3', 'ipst-epub.appspot.com');
		 ga('send', 'pageview');
		</script> 

	</body>
  
  	<% } else { %>
	
	<body>	
		<script>window.location.href='/login.html'</script>
	</body>
	<% } %>
	
</html>
