#!/bin/bash
handlebars book-templates-list-for-theme1.handlebars -f book-templates-list-for-theme1.js
handlebars book-templates-list-for-theme2.handlebars -f book-templates-list-for-theme2.js
handlebars book-templates-list-for-theme3.handlebars -f book-templates-list-for-theme3.js

handlebars ipst-epub-chapter-theme-1-template-1.handlebars -f ipst-epub-chapter-theme-1-template-1.js
handlebars ipst-epub-chapter-theme-1-template-2.handlebars -f ipst-epub-chapter-theme-1-template-2.js
handlebars ipst-epub-chapter-theme-1-template-3.handlebars -f ipst-epub-chapter-theme-1-template-3.js
handlebars ipst-epub-chapter-theme-1-template-4.handlebars -f ipst-epub-chapter-theme-1-template-4.js
handlebars ipst-epub-copyright-theme-1-template-1.handlebars -f ipst-epub-copyright-theme-1-template-1.js
handlebars ipst-epub-dedication-theme-1-template-1.handlebars -f ipst-epub-dedication-theme-1-template-1.js
handlebars ipst-epub-forward-theme-1-template-1.handlebars -f ipst-epub-forward-theme-1-template-1.js
handlebars ipst-epub-preface-theme-1-template-1.handlebars -f ipst-epub-preface-theme-1-template-1.js
handlebars ipst-epub-section-theme-1-template-1.handlebars -f ipst-epub-section-theme-1-template-1.js
handlebars ipst-epub-section-theme-1-template-2.handlebars -f ipst-epub-section-theme-1-template-2.js
handlebars ipst-epub-section-theme-1-template-3.handlebars -f ipst-epub-section-theme-1-template-3.js
handlebars ipst-epub-section-theme-1-template-4.handlebars -f ipst-epub-section-theme-1-template-4.js

handlebars ipst-epub-chapter-theme-2-template-1.handlebars -f ipst-epub-chapter-theme-2-template-1.js
handlebars ipst-epub-chapter-theme-2-template-2.handlebars -f ipst-epub-chapter-theme-2-template-2.js
handlebars ipst-epub-copyright-theme-2-template-1.handlebars -f ipst-epub-copyright-theme-2-template-1.js
handlebars ipst-epub-dedication-theme-2-template-1.handlebars -f ipst-epub-dedication-theme-2-template-1.js
handlebars ipst-epub-forward-theme-2-template-1.handlebars -f ipst-epub-forward-theme-2-template-1.js
handlebars ipst-epub-preface-theme-2-template-1.handlebars -f ipst-epub-preface-theme-2-template-1.js
handlebars ipst-epub-section-theme-2-template-1.handlebars -f ipst-epub-section-theme-2-template-1.js
handlebars ipst-epub-section-theme-2-template-2.handlebars -f ipst-epub-section-theme-2-template-2.js

handlebars ipst-epub-chapter-theme-3-template-1.handlebars -f ipst-epub-chapter-theme-3-template-1.js
handlebars ipst-epub-chapter-theme-3-template-2.handlebars -f ipst-epub-chapter-theme-3-template-2.js
handlebars ipst-epub-copyright-theme-3-template-1.handlebars -f ipst-epub-copyright-theme-3-template-1.js
handlebars ipst-epub-dedication-theme-3-template-1.handlebars -f ipst-epub-dedication-theme-3-template-1.js
handlebars ipst-epub-forward-theme-3-template-1.handlebars -f ipst-epub-forward-theme-3-template-1.js
handlebars ipst-epub-preface-theme-3-template-1.handlebars -f ipst-epub-preface-theme-3-template-1.js
handlebars ipst-epub-section-theme-3-template-1.handlebars -f ipst-epub-section-theme-3-template-1.js
handlebars ipst-epub-section-theme-3-template-2.handlebars -f ipst-epub-section-theme-3-template-2.js
