package th.ac.ipst.epub;

import java.io.IOException;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
@SuppressWarnings("serial")
public class ChapterDataStore {
	public static void insertChapter(String publicationId,String title){
		DatastoreService data = DatastoreServiceFactory.getDatastoreService();
		Entity chapter = new Entity("Chapter");
		chapter.setProperty("PublicationId",Long.parseLong(publicationId));
		chapter.setProperty("Title",title);
		System.out.print(data.put(chapter));
	}
	
	public static void deleteChapter(){
		
	}
}
