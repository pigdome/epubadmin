package th.ac.ipst.epub.jade;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import com.google.api.client.util.Charsets;
import com.google.common.io.Resources;

import de.neuland.jade4j.template.TemplateLoader;

// FileTemplateLoader and ReaderTemplateLoader don't work so we have to make a custom one..
public class CustomTemplateLoader implements TemplateLoader {
	
	private String path;
	
	
	public CustomTemplateLoader(String path) {
		this.path = path;
	}

	@Override
	public long getLastModified(String name) {
		return -1; // no idea how to get this, so return -1
	}

	@Override
	public Reader getReader(String name) throws IOException {
		StringReader reader=new StringReader(Resources.toString(Resources.getResource(path), Charsets.UTF_8));
		return reader;
	}

}