package th.ac.ipst.epub.gcs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import th.ac.ipst.epub.EpubPackage;
import th.ac.ipst.epub.EpubPackageConnector;
import th.ac.ipst.epub.Publication;
import th.ac.ipst.epub.PublicationConnector;
import th.ac.ipst.epub.jade.CustomTemplateLoader;
import th.ac.ipst.epub.util.EncodingUtil;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.neuland.jade4j.Jade4J;
import de.neuland.jade4j.JadeConfiguration;
import de.neuland.jade4j.exceptions.JadeException;
import de.neuland.jade4j.template.JadeTemplate;
import de.neuland.jade4j.template.TemplateLoader;

public final class EpubHelper {

	private static final Logger log = Logger.getLogger(EpubHelper.class.getName());

	private final static EpubHelper instance = new EpubHelper();

	private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
	
	/**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	
	private static final String BUCKET="ipst-epub";
	private static final String MIMETYPE_TEMPLATE_PATH="admin/epub-templates/mimetype";
	private static final String CONTAINER_TEMPLATE_PATH="admin/epub-templates/container.xml";
	
	private static final String GCS_BASE_URL="http://commondatastorage.googleapis.com/";
	
	/*
	// This is where backoff parameters are configured. Here it is aggressively retrying with
	// backoff, up to 10 times but taking no more that 15 seconds total to do so.
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	      .initialRetryDelayMillis(10)
	      .retryMaxAttempts(10)
	      .totalRetryPeriodMillis(15000)
	      .build());
	*/

	public static EpubHelper getInstance() { return instance; }
	
	/*
	public static void main(String[] args) throws Exception {
		System.out.println(EpubHelper.getInstance().getPackageLocalPath("12001/5242093484113920/v1/epub/META-INF/container.xml"));
		System.out.println(EpubHelper.getInstance().getPackageLocalPath("12001/5242093484113920/v1/epub/OPS/images/test.png"));
		
		EpubPackage epubPackage=new EpubPackage();
		epubPackage.setIdentifier("id");
		epubPackage.setTitle("title");
		epubPackage.setLanguage("th");
		ArrayList<String> ids=new ArrayList<String>();
		ids.add("i1");ids.add("i2");
		epubPackage.setItemIds(ids);
		ArrayList<String> properties=new ArrayList<String>();
		properties.add("p1");properties.add("p2");
		epubPackage.setItemProperties(properties);
		ArrayList<String> hrefs=new ArrayList<String>();
		hrefs.add("h1");hrefs.add("h2");
		epubPackage.setItemHrefs(hrefs);
		ArrayList<String> mediaTypes=new ArrayList<String>();
		mediaTypes.add("m1");mediaTypes.add("m2");
		epubPackage.setItemMediaTypes(mediaTypes);
		
		ArrayList<String> idrefs=new ArrayList<String>();
		idrefs.add("r1");idrefs.add("r2");		
		epubPackage.setItemrefIdRefs(idrefs);
		ArrayList<String> linears=new ArrayList<String>();
		linears.add("l1");linears.add("l2");		
		epubPackage.setItemrefLinears(linears);

		Gson gson=new GsonBuilder().create();
		Type type =new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> model = gson.fromJson(gson.toJson(epubPackage), type);
		
		TemplateLoader loader = new CustomTemplateLoader("tmpl/package.jade");
		JadeConfiguration config = new JadeConfiguration();
		config.setTemplateLoader(loader);
		config.setPrettyPrint(true);
		//config.setMode(Jade4J.Mode.HTML);   // <input checked>
		//config.setMode(Jade4J.Mode.XHTML);  // <input checked="true" />     ไม่เห็น work เลย สงสัยได้เฉพาะ tag html จริงๆ custom tag ไม่ได้
		//config.setMode(Jade4J.Mode.XML);    // <input checked="true"></input>
		JadeTemplate template=config.getTemplate(null);
		String html = config.renderTemplate(template, model);
		
		System.out.println(html);
	}
	*/
	
	private EpubHelper() 
	{
	}

	public ArrayList<String> getAllHtmlsInSpineOrder(Long publicationId) throws IOException
	{
		ArrayList<String> htmlStrings=new ArrayList<String>();
		
		EpubPackage epubPackage=EpubPackageConnector.getByPublicationId(publicationId);
		Publication publication=PublicationConnector.getById(publicationId);
		
		// process html files in spine order, skipping non-linear items
		ArrayList<String> itemrefIdRefs=epubPackage.getItemrefIdRefs();
		ArrayList<String> itemrefLinears=epubPackage.getItemrefLinears();
		
		for(int i=0; i<itemrefLinears.size(); i++) {
			String itemrefLinear=itemrefLinears.get(i);
			if(!itemrefLinear.equals("yes"))
				continue;
			
			String htmlHref=itemrefIdRefs.get(i)+".html";
			GcsInputChannel inputChannel = this.gcsService.openPrefetchingReadChannel(new GcsFilename(BUCKET, publication.getGcsEpubPackagePath() + "OPS/" + htmlHref), 0, BUFFER_SIZE);
			String htmlString=IOUtils.toString(Channels.newInputStream(inputChannel), "UTF-8");
			
			htmlStrings.add(htmlString);
		}

		return htmlStrings;
	}
	
	public void postMimetypeFile(String path) throws IOException 
	{ 
		GcsInputChannel inputChannel = this.gcsService.openPrefetchingReadChannel(new GcsFilename(BUCKET, MIMETYPE_TEMPLATE_PATH), 0, BUFFER_SIZE);
		GcsOutputChannel outputChannel = this.gcsService.createOrReplace(new GcsFilename(BUCKET, path+"mimetype"), GcsFileOptions.getDefaultInstance());
		copy(Channels.newInputStream(inputChannel), Channels.newOutputStream(outputChannel));
	}

	public void postContainerFile(String path) throws IOException 
	{ 
		GcsInputChannel inputChannel = this.gcsService.openPrefetchingReadChannel(new GcsFilename(BUCKET, CONTAINER_TEMPLATE_PATH), 0, BUFFER_SIZE);
		GcsOutputChannel outputChannel = this.gcsService.createOrReplace(new GcsFilename(BUCKET, path+"META-INF/container.xml"), GcsFileOptions.getDefaultInstance());
		copy(Channels.newInputStream(inputChannel), Channels.newOutputStream(outputChannel));
	}

	/**
	 * Transfer the data from the inputStream to the outputStream. Then close both streams.
	 */
	private void copy(InputStream input, OutputStream output) throws IOException {
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = input.read(buffer);
			while (bytesRead != -1) {
				output.write(buffer, 0, bytesRead);
				bytesRead = input.read(buffer);
			}
		} finally {
			input.close();
			output.close();
		}
	}
	
	

	/*
	private String getPackageLocalPath(String gcsFile)
	{
		String localPath=gcsFile.replaceAll("^.*epub/", ""); // http://stackoverflow.com/questions/15594780/java-replace-regex-not-working
		return localPath;
	}
	*/

	private boolean exists(GcsFilename gcsFilename) throws IOException
	{
		boolean exists=true;
        GcsFileMetadata meta = this.gcsService.getMetadata(gcsFilename);
        if (meta == null) {
            exists=false;
        }
        //int fileSize = (int) meta.getLength();
        //  LOGGER.fine("adding " + file.toString());
        return exists;
	}
	
	// some zip entries don't come from GCS, but dynamically initialized as a string
	private void addZipEntryFromString(ZipOutputStream zip, String content, String destination) throws IOException
	{
		if(content==null)
			return;
		
        try {
            ZipEntry entry = new ZipEntry(destination);
            zip.putNextEntry(entry);
            zip.write(content.getBytes("UTF-8"));
        } finally {
            zip.closeEntry();
        }
	}
	
	private void addZipEntry(ZipOutputStream zip, GcsFilename source, String destination) throws IOException 
	{
	    final int fetchSize = 4 * 1024 * 1024;
	    final int readSize = 2 * 1024 * 1024;

    	if(!exists(source)) {
            log.severe(source.toString() + " NOT FOUND. Skipping.");
    		return;
    	}

	    GcsInputChannel readChannel = null;
        try {
        	
            ZipEntry entry = new ZipEntry(destination);
            zip.putNextEntry(entry);
            readChannel = this.gcsService.openPrefetchingReadChannel(source, 0, fetchSize);
            final ByteBuffer buffer = ByteBuffer.allocate(readSize);
            int bytesRead = 0;
            while (bytesRead >= 0) {
                bytesRead = readChannel.read(buffer);
                buffer.flip();
                zip.write(buffer.array(), buffer.position(), buffer.limit());
                buffer.rewind();
                buffer.limit(buffer.capacity());
            } 

        }
        catch(Exception e) {
        	log.severe(e.getLocalizedMessage()); // IMPORTANT: กรณีที่ entry ซ้ำกัน เกิดได้กรณี export epub ออกมา import epub กลับเข้าไป แล้วพยายามจะ export กลับมาใหม่ entry ของ system media files จะซ้ำกัน เพราะได้มาจากตอน import ด้วย... TODO: หาวิธีจัดการ system media files ให้ดีกว่านี้!
        }
        finally {
            log.severe(source.toString()+" "+destination);
            if(zip!=null)
            	zip.closeEntry();
            if(readChannel!=null)
            	readChannel.close();
        }
	}

	private String getMediaMimetypeFromHref(String href)
	{
		Pattern pattern = Pattern.compile("^([a-z]+)s/.+\\.([a-z0-9A-Z]+)$");
		Matcher matcher = pattern.matcher(href);
		matcher.find();
		String mediaType=matcher.group(1);
		String fileExtension=matcher.group(2);

		String mimetype=mediaType+"/"+fileExtension;
		return mimetype;
	}
	
	private void writeToGcs(InputStream in, String outGcsKey, String mimetype) throws IOException
	{
        GcsFileOptions options = new GcsFileOptions.Builder().mimeType(mimetype).build();
		GcsOutputChannel outputChannel = this.gcsService.createOrReplace(new GcsFilename(BUCKET, outGcsKey), options);
		OutputStream out=Channels.newOutputStream(outputChannel);
		
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = in.read(buffer);
			while (bytesRead != -1) {
				out.write(buffer, 0, bytesRead);
				bytesRead = in.read(buffer);
			}
		} finally {
			//in.close(); // ZipInputStream has to remain open throughout the write session
			out.close();
		}
	}
	
	private String getStringFromInputStream(InputStream is) 
	{	 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is, "UTF-8") {
				// IMPORTANT: can't use the default implementation as it seems to close input stream afterwards
				// http://stackoverflow.com/questions/3229848/how-do-i-avoid-closing-an-inputstream-passed-to-my-method-that-i-wrap-in-reader
				public void close () {
				    // Do nothing.
				  }
			});
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
	}
	
	private void parsePackageOpf(InputStream is, Publication publication, EpubPackage epubPackage)
	{
		/* jsoup doesn't support namespace!!!
		String packageOpf=getStringFromInputStream(is);
		Document doc = Jsoup.parse(packageOpf, "UTF-8", Parser.xmlParser());
		
		Element coverImageItem=doc.select("package > manifest > item[properties='cover-image']").first();
		String coverUrl=GCS_BASE_URL + BUCKET + "/" + publication.getAuthorId() + "/" + "covers" + "/" + coverImageItem.attr("href").split("images/")[1];		
		publication.setCoverUrl(coverUrl);
		
		Element titleItem=doc.select("package > metadata > dc:title").first();
		String title=titleItem.text();				
		publication.setTitle(title);
		*/
		
		DocumentBuilder builder = null;
		try {
			DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
			builder = builderFactory.newDocumentBuilder();

			//document = builder.parse(is); // IMPORTANT: can't use this as it seems to close input stream afterwards
			// http://stackoverflow.com/questions/3229848/how-do-i-avoid-closing-an-inputstream-passed-to-my-method-that-i-wrap-in-reader
			// other relevant links:
			// http://stackoverflow.com/questions/562160/in-java-how-do-i-parse-xml-as-a-string-instead-of-a-file
			// http://stackoverflow.com/questions/20020982/java-create-inputstream-from-zipinputstream-entry/20021172#20021172
			// http://stackoverflow.com/questions/9578815/does-documentbuilder-parse-close-the-inputstream
			// http://stackoverflow.com/questions/8341680/java-saxparser-keep-inputstream-open
			Document document=builder.parse(new InputSource(new StringReader(getStringFromInputStream(is))));

			XPath xpath = XPathFactory.newInstance().newXPath();

			String coverUrlExpression = "/package/manifest/item[@properties='cover-image']";
			Element coverUrlElement = (Element) xpath.evaluate(coverUrlExpression, document, XPathConstants.NODE);
			String coverUrl=GCS_BASE_URL + BUCKET + "/" + publication.getAuthorId() + "/" + "covers" + "/" + coverUrlElement.getAttribute("href").split("cover/")[1];
			publication.setCoverUrl(coverUrl);
			
			String titleExpression = "/package/metadata/title"; // IMPORTANT: not use dc:title because xml namespace is a bit difficult to set up, so ignore it currently			
			Element titleElement = (Element) xpath.evaluate(titleExpression, document, XPathConstants.NODE);
			String title=titleElement.getTextContent();
			epubPackage.setTitle(title);
			publication.setTitle(title);

			String identifierExpression = "/package/metadata/identifier"; 
			Element identifierElement = (Element) xpath.evaluate(identifierExpression, document, XPathConstants.NODE);
			String identifier=identifierElement.getTextContent();
			epubPackage.setIdentifier(identifier);

			String languageExpression = "/package/metadata/language"; 
			Element languageElement = (Element) xpath.evaluate(languageExpression, document, XPathConstants.NODE);
			String language=languageElement.getTextContent();
			epubPackage.setLanguage(language);
			
			String spineExpression="/package/spine";
			Element spineElement = (Element) xpath.evaluate(spineExpression, document, XPathConstants.NODE);
			NodeList itemrefs=spineElement.getChildNodes();
			for(int i=0; i<itemrefs.getLength(); i++) {
				if (itemrefs.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element itemrefElement=(Element) itemrefs.item(i);
					epubPackage.addItemrefIdRef(itemrefElement.getAttribute("idref"));
					epubPackage.addItemrefLinear(itemrefElement.getAttribute("linear"));
				}
			}
			
		} catch (SAXException | IOException | ParserConfigurationException | XPathExpressionException e) {
			// TODO Auto-generated catch block
			log.severe(e.getLocalizedMessage());
		}
	}

	private void parseNavHtml(InputStream is, Publication publication, EpubPackage epubPackage)
	{
		DocumentBuilder builder = null;
		try {
			DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
			builder = builderFactory.newDocumentBuilder();

			Document document=builder.parse(new InputSource(new StringReader(getStringFromInputStream(is))));

			XPath xpath = XPathFactory.newInstance().newXPath();

			// set themeId ที่แอบใส่ไว้ใน <body> ของ nav.html
			String bodyExpression = "/html/body"; 
			Element bodyElement = (Element) xpath.evaluate(bodyExpression, document, XPathConstants.NODE);
			String themeId=bodyElement.getAttribute("data-theme-id");
			publication.setThemeId(themeId);
			
			String navExpression = "/html/body/nav/div";
			Element navElement = (Element) xpath.evaluate(navExpression, document, XPathConstants.NODE);

			DOMImplementationLS domImplLS = (DOMImplementationLS) document.getImplementation();
			LSSerializer serializer = domImplLS.createLSSerializer();
			String tocHtmlString = serializer.writeToString(navElement).replaceAll("^<\\?xml version=\"1\\.0\" encoding=\"UTF\\-16\"\\?>", "");
			
			// store navHtmlString just in case
			epubPackage.setNavHtmlString(new Text(tocHtmlString));
			
			// remove links for display in the editor (with jQuery Nestable)
			tocHtmlString=tocHtmlString.replaceAll("<a .*?>", "");
			tocHtmlString=tocHtmlString.replaceAll("</a>", "");
			
			epubPackage.setTocHtmlString(new Text(tocHtmlString));

		} catch (SAXException | IOException | ParserConfigurationException | XPathExpressionException e) {
			// TODO Auto-generated catch block
			log.severe(e.getLocalizedMessage());
		}
	}

	private EpubPackage getNewEpubPackage()
	{
    	EpubPackage epubPackage = new EpubPackage();

		epubPackage.setCoverItemId("cover-image");
		epubPackage.setCoverItemProperties("cover-image");
		epubPackage.setNavItemId("nav");
		epubPackage.setNavItemProperties("nav");

		return epubPackage;
	}
	
	private void manageMediaItem(EpubPackage epubPackage, String filePath)
	{
		Pattern pattern = Pattern.compile("^([a-z]+)s/(.+)\\.([a-z0-9A-Z]+)$");
		Matcher matcher = pattern.matcher(filePath);
		matcher.find();
		String mediaType=matcher.group(1);
		String mediaItemId=matcher.group(2);
		String fileExtension=matcher.group(3);
		String mediaItemMediaType=mediaType+"/"+fileExtension;
						
		epubPackage.addMediaItemId(mediaItemId);
		epubPackage.addMediaItemMediaType(mediaItemMediaType);
		epubPackage.addMediaItemHref(filePath);
	}

	private void manageHtmlItem(EpubPackage epubPackage, String href)
	{
		String itemId=href.split(".html$")[0];
		String itemMediaType="text/html";
		String itemHref=href;
		String itemProperty="";
		epubPackage.addItemId(itemId);
		epubPackage.addItemMediaType(itemMediaType);
		epubPackage.addItemHref(itemHref);
		epubPackage.addItemProperty(itemProperty);
	}

	public List extractFilesFromEpub(String epubGcsKey, Long authorId) throws IOException 
	{
		// create a new, empty publication entity
		Publication newPublication=new Publication();
		newPublication.setAuthorId(authorId);
		Publication insertedPublication=PublicationConnector.insert(newPublication);
		
		GcsFilename epubGcsFileName=new GcsFilename(BUCKET, epubGcsKey);
	    GcsInputChannel inputChannel = null;
	    inputChannel=this.gcsService.openPrefetchingReadChannel(epubGcsFileName, 0, BUFFER_SIZE);
		
		//get the zip file content
    	ZipInputStream zis = new ZipInputStream(Channels.newInputStream(inputChannel));
    	//get the zipped file list entry
    	ZipEntry ze = zis.getNextEntry();
    	
    	String epubPackageRootPath=insertedPublication.getAuthorId().toString()+"/"+insertedPublication.getId().toString()+"/"+"v1"+"/"+"epub"+"/";
    	String mediaPath=insertedPublication.getAuthorId().toString()+"/"+"media"+"/";
    	String bookCoverPath=insertedPublication.getAuthorId().toString()+"/"+"covers"+"/";
    	
    	EpubPackage epubPackage = getNewEpubPackage();
    	
    	while(ze!=null) {
    		if (ze.isDirectory()) {
                ze = zis.getNextEntry();
                continue;
            }

    		String filePath=ze.getName();

    		// skip OPS/css/*
    		if(filePath.startsWith("OPS/css/")) {
                ze = zis.getNextEntry();
    			continue;
    		}
    		
    		String destinationPath = null;

    		// parse package.opf, e.g., to locate the book cover
    		if(filePath.startsWith("OPS/package.opf")) { // no need to store package.opf in GCS because it is dynamically generated at export time
    			parsePackageOpf(zis, insertedPublication, epubPackage);
    		} 
    		// parse nav.html to get tocHtmlString and navHtmlString to store in EpubPackage entity
    		else if(filePath.startsWith("OPS/nav.html")) { // no need to store package.opf in GCS because it is dynamically generated at export time
    			parseNavHtml(zis, insertedPublication, epubPackage);
    		}
    		else if(filePath.startsWith("OPS/images/") || filePath.startsWith("OPS/videos/") || filePath.startsWith("OPS/audios/")) {
    			String href=filePath.split("OPS/")[1];
    			destinationPath=mediaPath+href;
    			String mimetype=getMediaMimetypeFromHref(href);
        		writeToGcs(zis, destinationPath, mimetype);
        		manageMediaItem(epubPackage, href); // TODO: จริงๆ ต้อง parse package.opf
    		} 
    		else if(filePath.startsWith("OPS/cover/") ) {
    			String href=filePath.split("OPS/cover/")[1];
    			destinationPath=bookCoverPath+href;
        		writeToGcs(zis, destinationPath, "image/png");
    		}
    		else if(filePath.equals("META-INF/container.xml")) {
    			destinationPath=epubPackageRootPath+filePath;
        		writeToGcs(zis, destinationPath, "text/xml");
    		}
        	else if(filePath.equals("mimetype")) {    			
    			destinationPath=epubPackageRootPath+filePath;
        		writeToGcs(zis, destinationPath, "text/plain");	
    		}
    		else { // *.html
    			destinationPath=epubPackageRootPath+filePath;
        		writeToGcs(zis, destinationPath, "text/html");
    			String href=filePath.split("OPS/")[1];
        		manageHtmlItem(epubPackage, href); // TODO: จริงๆ ต้อง parse package.opf
    		}
    		    		
    		zis.closeEntry();
            ze=zis.getNextEntry();
        }
        zis.close();        
        
		Publication updatedPublication=PublicationConnector.update(insertedPublication);
		
		epubPackage.setPublicationId(updatedPublication.getId());
		EpubPackage insertedEpubPackage=EpubPackageConnector.insert(epubPackage);

		// delete the uploaded file
		this.gcsService.delete(epubGcsFileName);

		return Arrays.asList(updatedPublication, insertedEpubPackage);
	}
	
	// TODO: ต้องเขียน function เพิ่มหา local path สำหรับพวก media files จาก urls (getPackageLocalPath ใช้ได้กับแค่ html file)
	private void zipFilesAsEpub(GcsFilename targetZipFile, 
								ArrayList<GcsFilename> sources, 
								ArrayList<String> destinations,
								String packageSource, String packageDestination,
								String navSource, String navDestination) throws IOException {

	    GcsOutputChannel outputChannel = null;
	    ZipOutputStream zip = null;
	    try {
	        GcsFileOptions options = new GcsFileOptions.Builder().mimeType("application/epub+zip").build();
	        outputChannel = this.gcsService.createOrReplace(targetZipFile, options);
	        zip = new ZipOutputStream(Channels.newOutputStream(outputChannel));
	        
	        addZipEntryFromString(zip, packageSource, packageDestination);
	        addZipEntryFromString(zip, navSource, navDestination);
	        
	        for(int i=0; i<sources.size(); i++) {
	        	GcsFilename source=sources.get(i);
	        	String destination=destinations.get(i);
	        	addZipEntry(zip, source, destination);
	        }
	    } finally {
	        zip.flush();
	        zip.close();
	        outputChannel.close();
	    }
	}
	
	/*
	// Error for /epubpackageendpoint
	// java.lang.NoClassDefFoundError: sun.misc.Unsafe is a restricted class. Please see the Google App Engine developer's guide for more details.
	// at com.google.apphosting.runtime.security.shared.stub.sun.misc.Unsafe.<clinit>(Unsafe.java)
	private String applyTemplate(String templatePath, Object entity) throws JadeException, IOException
	{
		Gson gson=new GsonBuilder().create();
		Type type =new TypeToken<Map<String, Object>>(){}.getType();
		Map<String, Object> model = gson.fromJson(gson.toJson(entity), type);

		TemplateLoader loader = new CustomTemplateLoader(templatePath);
		JadeConfiguration config = new JadeConfiguration();
		config.setTemplateLoader(loader);
		config.setPrettyPrint(true);
		//config.setMode(Jade4J.Mode.HTML);   // <input checked>
		//config.setMode(Jade4J.Mode.XHTML);  // <input checked="true" />     ไม่เห็น work เลย สงสัยได้เฉพาะ tag html จริงๆ custom tag ไม่ได้
		//config.setMode(Jade4J.Mode.XML);    // <input checked="true"></input>
		JadeTemplate template=config.getTemplate(null);
		
		return config.renderTemplate(template, model);		
	}
	*/
	
	public boolean genEpub(Publication publication, EpubPackage epubPackage) throws IOException 
	{
		boolean succeeded=true;

		String gcsEpubPackagePath=publication.getGcsEpubPackagePath();
		String gcsEpubMediaRootPath=publication.getMediaRootPath();
		String epubUrl=publication.getEpubRelativeUrl();

		ArrayList<GcsFilename> sources=new ArrayList<GcsFilename>();		
		ArrayList<String> destinations=new ArrayList<String>();		

		// mimetype
        sources.add(new GcsFilename(BUCKET, gcsEpubPackagePath+"mimetype"));
        destinations.add("mimetype");
        
        // container.xml
        sources.add(new GcsFilename(BUCKET, gcsEpubPackagePath+"META-INF/container.xml"));
        destinations.add("META-INF/container.xml");

        /* can't do this the same way with others; package.opf and nav.html are generated dynamically from EpubPackage entity so no need to store in GCS
        // package.opf
        sources.add(new GcsFilename(BUCKET, gcsEpubPackagePath+"OPS/package.opf"));
        destinations.add("META-INF/package.opf");
        
        // nav.xhtml
        sources.add(new GcsFilename(BUCKET, gcsEpubPackagePath+"OPS/nav.html"));
        destinations.add("OPS/nav.html");
        */
        /* ใช้ jade4j ไม่ได้ ติด sandbox!
        String packageSource=applyTemplate("tmpl/package.jade", epubPackage);
        String packageDestination="META-INF/package.opf";
        String navSource=applyTemplate("tmpl/nav.jade", epubPackage);
        String navDestination="OPS/nav.html";
        */
        String coverHref="cover/"+publication.getCoverFileName(); // path นี้ไม่มี OPS แล้ว เพราะ package.opf อยู่ใน OPS เลยใช้แค่ relative path ก็พอ
        String packageSource=applyPackageTemplate(epubPackage, coverHref);
        String packageDestination="OPS/package.opf";
        String navSource=applyNavTemplate(epubPackage, publication.getThemeId());
        String navDestination="OPS/nav.html";
        
        // htmls
if(epubPackage.getItemHrefs()!=null) {        
        int htmlCount=epubPackage.getItemHrefs().size();
		for(int i=0; i<htmlCount; i++) {
			String htmlHref=epubPackage.getItemHrefs().get(i);
	        sources.add(new GcsFilename(BUCKET, gcsEpubPackagePath + "OPS/" + htmlHref));
	        destinations.add("OPS/"+htmlHref);
		}
}

		// book cover
		sources.add(new GcsFilename(BUCKET, publication.getCoverGcsObjectName()));
        destinations.add("OPS/cover/"+publication.getCoverFileName());

		// css (book templates)
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-1/ipst-epub-theme-1.css"));
        destinations.add("OPS/css/theme-1/ipst-epub-theme-1.css");
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-1/ipst-epub-theme-1-responsive.css"));
        destinations.add("OPS/css/theme-1/ipst-epub-theme-1-responsive.css");

        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-2/ipst-epub-theme-2.css"));
        destinations.add("OPS/css/theme-2/ipst-epub-theme-2.css");
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-2/ipst-epub-theme-2-responsive.css"));
        destinations.add("OPS/css/theme-2/ipst-epub-theme-2-responsive.css");

        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-3/ipst-epub-theme-3.css"));
        destinations.add("OPS/css/theme-3/ipst-epub-theme-3.css");
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-3/ipst-epub-theme-3-responsive.css"));
        destinations.add("OPS/css/theme-3/ipst-epub-theme-3-responsive.css");

        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-4/ipst-epub-theme-4.css"));
        destinations.add("OPS/css/theme-4/ipst-epub-theme-4.css");
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/css/theme-4/ipst-epub-theme-4-responsive.css"));
        destinations.add("OPS/css/theme-4/ipst-epub-theme-4-responsive.css");

        // TODO: จะ add พวก css/js/images แบบทื่อๆ แบบนี้ไม่ดี ควร add ผ่าน EpubPackage จะได้ส่งต่อไปถึง manifest ด้วย
        
		// css (bootstrap)
        sources.add(new GcsFilename(BUCKET, "admin/bootstrap/css/bootstrap.min.css"));
        destinations.add("OPS/css/bootstrap/bootstrap.min.css");        
        sources.add(new GcsFilename(BUCKET, "admin/bootstrap/css/bootstrap-responsive.min.css"));
        destinations.add("OPS/css/bootstrap/bootstrap-responsive.min.css");        
        
        /* ยังไม่ได้ใช้
        // js (jquery)
        sources.add(new GcsFilename(BUCKET, "admin/jquery/js/jquery-1.9.1.min.js"));
        destinations.add("OPS/js/jquery/jquery-1.9.1.min.js");        
        
        // js (bootstrap)
        sources.add(new GcsFilename(BUCKET, "admin/bootstrap/js/bootstrap.min.js"));
        destinations.add("OPS/js/bootstrap/bootstrap.min.js");        
        */
        
        // static graphics
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/bg-pattern-header-6.png"));
        destinations.add("OPS/images/bg-pattern-header-6.png");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/bg-pattern-header-8.png"));
        destinations.add("OPS/images/bg-pattern-header-8.png");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/bootstrap-mdo-sfmoma-01.jpg"));
        destinations.add("OPS/images/bootstrap-mdo-sfmoma-01.jpg");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/line-header-10.png"));
        destinations.add("OPS/images/line-header-10.png");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/line-header-11.png"));
        destinations.add("OPS/images/line-header-11.png");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/vector-img.png"));
        destinations.add("OPS/images/vector-img.png");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/vector-img-2.jpg"));
        destinations.add("OPS/images/vector-img-2.jpg");        
        sources.add(new GcsFilename(BUCKET, "admin/epub-templates/images/vector-img-4.jpg"));
        destinations.add("OPS/images/vector-img-4.jpg");        
        
		// media
if(epubPackage.getMediaItemHrefs()!=null) {        
		int mediaCount=epubPackage.getMediaItemHrefs().size();
		for(int i=0; i<mediaCount; i++) {
			String mediaHref=epubPackage.getMediaItemHrefs().get(i);
			/* no need anymore.. this is gcs client lib's bug!!!
			 * wrongly encoded a space with + instead of %20
			 * https://code.google.com/p/appengine-gcs-client/issues/detail?id=21
			 */
			/*
			String mediaHrefPath=mediaHref.split("/")[0]; // TODO: improve this ugly solution for encoding
			String mediaHrefFileName=mediaHref.split("/")[1];
			String encodedMediaHref=mediaHrefPath + "/" + EncodingUtil.encodeURIComponent(mediaHrefFileName);
			sources.add(new GcsFilename(BUCKET, gcsEpubMediaRootPath + encodedMediaHref)); // mediaHref already includes "images/", "videos/", or "audios/" part				
			//sources.add(new GcsFilename(BUCKET, EncodingUtil.encodeURIComponent(gcsEpubMediaRootPath + mediaHref))); // mediaHref already includes "images/", "videos/", or "audios/" part				
			*/
			sources.add(new GcsFilename(BUCKET, gcsEpubMediaRootPath + mediaHref)); // mediaHref already includes "images/", "videos/", or "audios/" part				
			destinations.add("OPS/"+mediaHref);
		}
}
		
		zipFilesAsEpub(new GcsFilename(BUCKET, epubUrl), 
						sources, destinations, 
						packageSource, packageDestination,
						navSource, navDestination);

		return succeeded;
	}
	
	private String applyPackageTemplate(EpubPackage epubPackage, String coverHref)
	{
		/* Jade specification (jade4j)
		package(xmlns="http://www.idpf.org/2007/opf", version="3.0", xml:lang="en", unique-identifier="pub-id")
		  metadata(xmlns:dc="http://purl.org/dc/elements/1.1/")
		    if locals.title
		      dc:title(id="title") #{title}
		    if locals.identifier
		      dc:identifier(id="pub-id") #{identifier}
		    if locals.language
		      dc:language #{language}
		    if locals.source
		      dc:source #{source}

		  manifest
		    item(id="#{navItemId}, properties="#{navItemProperties}", href="#{navItemHref}", media-type="#{navItemMediaType}")
		    item(id="#{coverItemId}, properties="#{coverItemProperties}", href="#{coverItemHref}", media-type="#{coverItemMediaType}")
		    each mediaItemId, i in mediaItemIds
		      item(id="#{mediaItemId}", href="#{mediaItemHrefs[i]}", media-type="#{mediaItemMediaTypes[i]}")
		    each itemId, i in itemIds
		      item(id="#{itemId}", properties="#{itemProperties[i]}", href="#{itemHrefs[i]}", media-type="#{itemMediaTypes[i]}")
		      
		  spine(page-progression-direction="ltr")
		    each itemrefIdRef, i in itemrefIdRefs
		      itemref(idref="#{itemrefIdRef}",linear="#{itemrefLinears[i]}")
		*/
		String packageHtmlString="";
		packageHtmlString+="<package xmlns='http://www.idpf.org/2007/opf' version='3.0' xml:lang='en' unique-identifier='pub-id'>" +"\n";
		
		packageHtmlString+="  <metadata xmlns:dc='http://purl.org/dc/elements/1.1/'>" +"\n";
		packageHtmlString+="    <dc:title id='title'>"+epubPackage.getTitle()+"</dc:title>"+"\n";
		packageHtmlString+="    <dc:identifier id='pub-id'>"+epubPackage.getIdentifier()+"</dc:identifier>"+"\n";
		packageHtmlString+="    <dc:language>"+epubPackage.getLanguage()+"</dc:language>"+"\n";
		packageHtmlString+="  </metadata>"+"\n";

		packageHtmlString+="  <manifest>"+"\n";
		packageHtmlString+="    <item id='"+epubPackage.getNavItemId()+"' properties='"+epubPackage.getNavItemProperties()+"' href='"+epubPackage.getNavItemHref()+"' media-type='"+epubPackage.getNavItemMediaType()+"' />"+"\n";
		packageHtmlString+="    <item id='"+epubPackage.getCoverItemId()+"' properties='"+epubPackage.getCoverItemProperties()+"' href='"+coverHref+"' media-type='"+epubPackage.getCoverItemMediaType()+"' />"+"\n";
if(epubPackage.getMediaItemHrefs()!=null) {
	int mediaCount=epubPackage.getMediaItemHrefs().size(); 	
	for(int i=0; i<mediaCount; i++) {
		packageHtmlString+="    <item id='"+epubPackage.getMediaItemIds().get(i)+"' href='"+epubPackage.getMediaItemHrefs().get(i)+"' media-type='"+epubPackage.getMediaItemMediaTypes().get(i)+"' />"+"\n";
	}
}
if(epubPackage.getItemHrefs()!=null) {
	int htmlCount=epubPackage.getItemHrefs().size(); 	
	for(int i=0; i<htmlCount; i++) {
		packageHtmlString+="    <item id='"+epubPackage.getItemIds().get(i)+"' properties='"+epubPackage.getItemProperties()+"' href='"+epubPackage.getItemHrefs().get(i)+"' media-type='"+epubPackage.getItemMediaTypes().get(i)+"' />"+"\n";	
	}
}	
		packageHtmlString+="  </manifest>"+"\n";
		
		packageHtmlString+="  <spine page-progression-direction='ltr'>"+"\n"; // fixed to left-to-right language only for the time being
if(epubPackage.getItemrefIdRefs()!=null) {
	int itemRefCount=epubPackage.getItemrefIdRefs().size(); 	
	for(int i=0; i<itemRefCount; i++) {
		packageHtmlString+="    <itemref idref='"+epubPackage.getItemrefIdRefs().get(i)+"' linear='"+epubPackage.getItemrefLinears().get(i)+"' />"+"\n";
	}
}
		packageHtmlString+="  </spine>"+"\n";
		
		packageHtmlString+="</package>"+"\n";
		return packageHtmlString;
	}
	
	private String applyNavTemplate(EpubPackage epubPackage, String themeId)
	{
		/*
		!!! xml
		html(xmlns="http://www.w3.org/1999/xhtml", xmlns:epub="http://www.idpf.org/2007/opf")
		  head
		    title สารบัญ
		
		  body
		    if locals.tocHtmlString
		      nav(epub:type="toc")
		        h2 Contents
		        !{tocHtmlString}
		      
		    if locals.landmarksHtmlString
		      nav(epub:type="landmarks")
		        h2 Landmarks
		        !{landmarksHtmlString}
		 */
		
		String navHtmlString="";
		navHtmlString+="<html xmlns='http://www.w3.org/1999/xhtml' xmlns:epub='http://www.idpf.org/2007/opf'>" +"\n";
		navHtmlString+="  <head>"+"\n";
		navHtmlString+="    <title>สารบัญ</title>"+"\n";		
		navHtmlString+="  </head>"+"\n";
		navHtmlString+="  <body data-theme-id='"+themeId+"'>"+"\n"; // IMPORTANT: แอบใส่ themeId ไว้ตรงนี้เพื่อตอน import กลับเข้ามา จะได้ set ค่าใน Publication entity ได้ถูก
		navHtmlString+="    <nav>"+"\n";
		navHtmlString+="      <h2>สารบัญ (Contents)</h2>"+"\n";		
	String toc=epubPackage.getNavHtmlString().getValue(); // IMPORTANT: use .getNavHtmlString which contains links rather than .getTocHtmlString which is used in the editor only
	if(toc!=null && !toc.trim().equals(""))	{
		navHtmlString+="    "+toc+"\n";		
	}
		navHtmlString+="    </nav>"+"\n";
		navHtmlString+="  </body>"+"\n";
		navHtmlString+="</html>";
		return navHtmlString;
	}
}
