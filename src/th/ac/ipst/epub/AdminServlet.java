package th.ac.ipst.epub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import th.ac.ipst.epub.gcs.EpubHelper;
import th.ac.ipst.epub.gcs.UrlSigner;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class AdminServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		String method = req.getParameter("req");
		
		if(method.equals("listAuthor"))
		{
			String limitString=req.getParameter("limit");
			Integer limit=null; 
			if(limitString!=null)
				limit=Integer.valueOf(req.getParameter("limit"));
			List<Author> author=AuthorConnector.list(limit);
			System.out.print( author);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(author));
	        
		}else if(method.equals("listBook")){
			String limitString=req.getParameter("limit");
			Integer limit=null; 
			if(limitString!=null)
				limit=Integer.valueOf(req.getParameter("limit"));
			List<Publication> publications=PublicationConnector.list(limit);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(publications));
	        
		}else if(method.equals("search")){
			
	        
		}else if(method.equals("getAuthorById")){
			String idstring=req.getParameter("id");
			Long id=Long.parseLong(idstring); 
			Author author=AuthorConnector.getById(id);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(author));
			
		}else if(method.equals("getBookById")){
			String idstring=req.getParameter("id");
			Long id=Long.parseLong(idstring); 
			Publication book=PublicationConnector.getById(id);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(book));
	        
		}
		else if(method.equals("deleteAuthor")){
			System.out.print(555);
			Long authorId=Long.valueOf(req.getParameter("id"));
			AuthorConnector.remove(authorId);
		}else if(method.equals("deleteBook")){
			Long publicationId=Long.valueOf(req.getParameter("id"));
			PublicationConnector.remove(publicationId);
		}
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		String method = req.getParameter("req");
		if(method.equals("updateBook")){
			Publication publication=new Gson().fromJson(req.getParameter("item"), Publication.class);
			Publication updatedPublication=PublicationConnector.update(publication);			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(updatedPublication));
		}else if(method.equals("deleteBook")){
			Long publicationId=Long.valueOf(req.getParameter("id"));
			PublicationConnector.remove(publicationId);
		}else if(method.equals("updateAuthor")){
			JSONObject info =null;
			Author author = null;
			try {
				System.out.println(req.getParameter("item"));
				info=(JSONObject)new JSONParser().parse(req.getParameter("item"));
				JSONObject value =(JSONObject)new JSONParser().parse(info.get("userInfo").toString());
				info.put("userInfo", value.get("value"));
				author = new Author();
				author.setId((Long) info.get("id"));
				author.setAbout(new Text(info.get("about").toString()));
				author.setDateOfMember((Long)info.get("dateOfMember"));
				author.setDisplayName((String) info.get("displayName"));
				author.setEmail((String) info.get("email"));
				author.setEmailVerified((boolean) info.get("emailVerified"));
				author.setIdentityProvider((String) info.get("identityProvider"));
				author.setPublicationCount((Long) info.get("publicationCount"));
				author.setUserInfo(new Text(info.get("userInfo").toString()));
				System.out.print(info);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Author author=new Gson().fromJson(req.getParameter("item"), Author.class);
			Author updatedAuthor=AuthorConnector.update(author);			
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(new GsonBuilder().create().toJson(updatedAuthor));
		
		}else if(method.equals("deleteAuthor")){
			Long authorId=Long.valueOf(req.getParameter("id"));
			AuthorConnector.remove(authorId);
		}
		
	}

}
