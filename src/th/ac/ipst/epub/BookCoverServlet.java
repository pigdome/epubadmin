package th.ac.ipst.epub;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import th.ac.ipst.epub.gcs.CredentialHelper;
import th.ac.ipst.epub.gcs.UrlSigner;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class BookCoverServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(BookCoverServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("list")) {
			Long authorId=Long.valueOf(req.getParameter("authorId"));
			String cursorString=req.getParameter("cursor");
			String limitString=req.getParameter("limit");
			Integer limit=null; 
			if(limitString!=null)
				limit=Integer.valueOf(req.getParameter("limit"));
			List bookCoversWithCursor=BookCoverConnector.list(authorId, cursorString, limit);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(bookCoversWithCursor));
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("insert")) {
			BookCover bookCover=new Gson().fromJson(req.getParameter("bookCover"), BookCover.class);
			BookCoverConnector.insert(bookCover);
		}
	}
}
