package th.ac.ipst.epub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;
import th.ac.ipst.epub.gcs.UrlSigner;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class ChapterServlet extends HttpServlet {
	
	private static final String GCS_ROOT_URL="http://commondatastorage.googleapis.com/ipst-epub/";

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException 
	{
		Chapter chapter = new Chapter(req.getParameter("publicationId"),req.getParameter("title"));
		ChapterConnector.insert(chapter);
		
	}
}