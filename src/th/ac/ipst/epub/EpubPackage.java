package th.ac.ipst.epub;

import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class EpubPackage 
{
    @PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    Long publicationId;
    //String version; ไม่จำเป็นต้องรู้ version เพราะ query ได้จาก publicationId ซึ่งเป็น key ของ Publication (แต่ละ version ของหนังสือเล่มเดียวกันมี publicationId ไม่ซ้ำกัน ต่างจาก publicationGroupId ซึ่งซ้ำกันสำหรับหนังสือเล่มเดียวกันแต่ต่าง version)
    
    // information about <package />
    String title = "untitled"; 
    String identifier = "NO_ID";
    String language = "th"; 
    
    // manifest's <item /> for html
    ArrayList<String> itemIds;
    ArrayList<String> itemProperties;
    ArrayList<String> itemHrefs;
    ArrayList<String> itemMediaTypes;

    // manifest's <item /> for media including images, videos, audios <-- ต้องแยกออกมาแบบนี้เพื่อความสะดวกในการ instantiate ค่าใน Jade's template 
    ArrayList<String> mediaItemIds; // extracted from mediaItemHrefs for file name as id
    //ArrayList<String> mediaItemProperties; // สำหรับ media ไม่มี properties
    ArrayList<String> mediaItemHrefs;
    ArrayList<String> mediaItemMediaTypes;

    String navItemId = "nav";
    String navItemProperties = "nav";
    String navItemHref = "nav.html"; // TODO: จริงๆ ควรเป็น  .xhtml
    String navItemMediaType = "text/html"; // TODO: application/xhtml+xml

    String coverItemId = "cover-image";
    String coverItemProperties = "cover-image";
    //String coverItemHref; // defined by cover's filename uploaded by a user   ไป fetch ได้จาก Publication entity (มั่วๆ แฮะ)
    String coverItemMediaType = "image/png";

    // spine's <itemref />
    ArrayList<String> itemrefIdRefs;
    ArrayList<String> itemrefLinears;
    // end of information about <package />
    
    // information about navigation <nav />
    // toc
    Text navHtmlString; // for use in readers only, and being parsed when the epub file is imported back
    Text tocHtmlString; // for use in the editor only (store contents of jQuery's nestable)

    public void setNavHtmlString(Text navHtmlString) 
    {
    	this.navHtmlString=navHtmlString;
    }
    public Text getNavHtmlString() 
    {
    	return this.navHtmlString;
    }

    public void setTocHtmlString(Text tocHtmlString) 
    {
    	this.tocHtmlString=tocHtmlString;
    }
    public Text getTocHtmlString() 
    {
    	return this.tocHtmlString;
    }
    
    public String getCoverItemMediaType()
    {
    	return this.coverItemMediaType;
    }
    public void setCoverItemMediaType(String coverItemMediaType)
    {
    	this.coverItemMediaType=coverItemMediaType;
    }
    /*
    public String getCoverItemHref()
    {
    	return this.coverItemHref;
    }
    public void setCoverItemHref(String coverItemHref)
    {
    	this.coverItemHref=coverItemHref;
    }
    */
    public String getCoverItemProperties()
    {
    	return this.coverItemProperties;
    }
    public void setCoverItemProperties(String coverItemProperties)
    {
    	this.coverItemProperties=coverItemProperties;
    }
    public String getCoverItemId()
    {
    	return this.coverItemId;
    }
    public void setCoverItemId(String coverItemId)
    {
    	this.coverItemId=coverItemId;
    }    
    
    
    
    
    public String getNavItemMediaType()
    {
    	return this.navItemMediaType;
    }
    public void setNavItemMediaType(String navItemMediaType)
    {
    	this.navItemMediaType=navItemMediaType;
    }
    public String getNavItemHref()
    {
    	return this.navItemHref;
    }
    public void setNavItemHref(String navItemHref)
    {
    	this.navItemHref=navItemHref;
    }
    public String getNavItemProperties()
    {
    	return this.navItemProperties;
    }
    public void setNavItemProperties(String navItemProperties)
    {
    	this.navItemProperties=navItemProperties;
    }
    public String getNavItemId()
    {
    	return this.navItemId;
    }
    public void setNavItemId(String navItemId)
    {
    	this.navItemId=navItemId;
    }
    
    
    public void addMediaItemMediaType(String mediaItemMediaType) 
    {
    	if(this.mediaItemMediaTypes==null) {
    		this.mediaItemMediaTypes=new ArrayList<String>();
    	}
    	this.mediaItemMediaTypes.add(mediaItemMediaType);
    }
    public void setMediaItemMediaTypes(ArrayList<String> mediaItemMediaTypes)
    {
    	this.mediaItemMediaTypes=mediaItemMediaTypes;
    }
    public ArrayList<String> getMediaItemMediaTypes()
    {
    	return this.mediaItemMediaTypes;
    }

    public void addMediaItemHref(String mediaItemHref) 
    {
    	if(this.mediaItemHrefs==null) {
    		this.mediaItemHrefs=new ArrayList<String>();
    	}
    	this.mediaItemHrefs.add(mediaItemHref);
    }
    public void setMediaItemHrefs(ArrayList<String> mediaItemHrefs)
    {
    	this.mediaItemHrefs=mediaItemHrefs;
    }
    public ArrayList<String> getMediaItemHrefs()
    {
    	return this.mediaItemHrefs;
    }


    public void addMediaItemId(String mediaItemId) 
    {
    	if(this.mediaItemIds==null) {
    		this.mediaItemIds=new ArrayList<String>();
    	}
    	this.mediaItemIds.add(mediaItemId);
    }
    public void setMediaItemIds(ArrayList<String> mediaItemIds)
    {
    	this.mediaItemIds=mediaItemIds;
    }
    public ArrayList<String> getMediaItemIds()
    {
    	return this.mediaItemIds;
    }

    
    public void addItemrefLinear(String itemrefLinear) 
    {
    	if(this.itemrefLinears==null) {
    		this.itemrefLinears=new ArrayList<String>();
    	}
    	this.itemrefLinears.add(itemrefLinear);
    }    
    public void setItemrefLinears(ArrayList<String> itemrefLinears)
    {
    	this.itemrefLinears=itemrefLinears;
    }
    public ArrayList<String> getItemrefLinears()
    {
    	return this.itemrefLinears;
    }

    public void addItemrefIdRef(String itemrefIdRef) 
    {
    	if(this.itemrefIdRefs==null) {
    		this.itemrefIdRefs=new ArrayList<String>();
    	}
    	this.itemrefIdRefs.add(itemrefIdRef);
    }
    public void setItemrefIdRefs(ArrayList<String> itemrefIdRefs)
    {
    	this.itemrefIdRefs=itemrefIdRefs;
    }
    public ArrayList<String> getItemrefIdRefs()
    {
    	return this.itemrefIdRefs;
    }

    public void addItemMediaType(String itemMediaType) 
    {
    	if(this.itemMediaTypes==null) {
    		this.itemMediaTypes=new ArrayList<String>();
    	}
    	this.itemMediaTypes.add(itemMediaType);
    }
    public void setItemMediaTypes(ArrayList<String> itemMediaTypes)
    {
    	this.itemMediaTypes=itemMediaTypes;
    }
    public ArrayList<String> getItemMediaTypes()
    {
    	return this.itemMediaTypes;
    }

    public void addItemHref(String itemHref) 
    {
    	if(this.itemHrefs==null) {
    		this.itemHrefs=new ArrayList<String>();
    	}
    	this.itemHrefs.add(itemHref);
    }
    public void setItemHrefs(ArrayList<String> itemHrefs)
    {
    	this.itemHrefs=itemHrefs;
    }
    public ArrayList<String> getItemHrefs()
    {
    	return this.itemHrefs;
    }

    public void addItemProperty(String itemProperty) 
    {
    	if(this.itemProperties==null) {
    		this.itemProperties=new ArrayList<String>();
    	}
    	this.itemProperties.add(itemProperty);
    }
    public void setItemProperties(ArrayList<String> itemProperties)
    {
    	this.itemProperties=itemProperties;
    }
    public ArrayList<String> getItemProperties()
    {
    	return this.itemProperties;
    }

    public void addItemId(String itemId) 
    {
    	if(this.itemIds==null) {
    		this.itemIds=new ArrayList<String>();
    	}
    	this.itemIds.add(itemId);
    }
    public void setItemIds(ArrayList<String> itemIds)
    {
    	this.itemIds=itemIds;
    }
    public ArrayList<String> getItemIds()
    {
    	return this.itemIds;
    }
    
    public Long getId()
    {
    	return this.id;
    }

    public void setId(Long id)
    {
    	this.id=id;
    }

    public Long getPublicationId()
    {
    	return this.publicationId;
    }

    public void setPublicationId(Long publicationId)
    {
    	this.publicationId=publicationId;
    }

    public String getTitle()
    {
    	return this.title;
    }

    public void setTitle(String title)
    {
    	this.title=title;
    }

    public String getIdentifier()
    {
    	return this.identifier;
    }
    
    public void setIdentifier(String identifier)
    {
    	this.identifier=identifier;
    }
    
    public String getLanguage()
    {
    	return this.language;
    }
    
    public void setLanguage(String language)
    {
    	this.language=language;
    }
}
