package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class ChapterConnector {
	public static Chapter getById(Long id){
		PersistenceManager mgr = getPersistenceManager();
		Chapter chapter = null;
		try {
			chapter = mgr.getObjectById(Chapter.class, id);
		} finally {
			mgr.close();
		}
		return chapter;	
	}
	
	public static void insert(Chapter chapter) 
	{
		PersistenceManager mgr = getPersistenceManager();
		mgr.makePersistent(chapter);
		Chapter chap =mgr.detachCopy(chapter);
		System.out.print(chap);
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
