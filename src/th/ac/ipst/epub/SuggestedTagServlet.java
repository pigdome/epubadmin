package th.ac.ipst.epub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.servlet.http.*;

import th.ac.ipst.epub.gcs.EpubHelper;

import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class SuggestedTagServlet extends HttpServlet {
	
	private static final Integer MAX_SUGGESTED_TAGS_RETURNED = 10;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String request=req.getParameter("req");
		
		if(request.equals("list")) {
			String queryString=req.getParameter("queryString");
			List<SuggestedTag> suggestedTags=SuggestedTagConnector.list(queryString, MAX_SUGGESTED_TAGS_RETURNED);
	        resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write(new GsonBuilder().create().toJson(suggestedTags));
		} 
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String request=req.getParameter("req");
		
		if(request.equals("update")) {
			ArrayList<String> tagsToAdd=new Gson().fromJson(req.getParameter("tagsToAdd"), new TypeToken<ArrayList<String>>(){}.getType());
			ArrayList<String> tagsToRemove=new Gson().fromJson(req.getParameter("tagsToRemove"), new TypeToken<ArrayList<String>>(){}.getType());
			Long authorId=Long.valueOf(req.getParameter("authorId"));

			SuggestedTagConnector.updates(tagsToAdd, tagsToRemove, authorId);
		}
	}
		
}
