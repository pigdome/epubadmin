package th.ac.ipst.epub;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class PublicationConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	
	@SuppressWarnings("unchecked")
	public static List list(Long authorId, String cursorString, Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Publication> publications = null;
		List<Publication> detachedPublications=null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Publication.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
        	query.setOrdering("dateCreated desc");

    	    boolean isLatestVersion=true;
    	    query.setFilter("authorId==authorIdParam && isLatestVersion==isLatestVersionParam");
            query.declareParameters("Long authorIdParam, boolean isLatestVersionParam");
            
			publications = (List<Publication>) query.execute(authorId, isLatestVersion);
			cursor = JDOCursorHelper.getCursor(publications);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();
			
			detachedPublications=new ArrayList<Publication>(mgr.detachCopyAll(publications));

		} finally {
			mgr.close();
		}
		
		return Arrays.asList(detachedPublications, cursorString);
	}
	//My code//
	@SuppressWarnings("unchecked")
	public static List list(Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Publication> publications = null;
		List<Publication> detachedPublications=null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Publication.class);
			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
        	query.setOrdering("dateCreated desc");

    	    boolean isLatestVersion=true;
    	    query.setFilter("isLatestVersion==isLatestVersionParam");
            query.declareParameters("boolean isLatestVersionParam");
            
			publications = (List<Publication>) query.execute(isLatestVersion);
			cursor = JDOCursorHelper.getCursor(publications);
			
			detachedPublications=new ArrayList<Publication>(mgr.detachCopyAll(publications));

		} finally {
			mgr.close();
		}
		
		return Arrays.asList(detachedPublications);
	}
	//===========================================================================//
	
	
	public static Publication getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Publication publication = null;

		try {
			publication = mgr.getObjectById(Publication.class, id);
		} finally {
			mgr.close();
		}
		return publication;		
	}
		
	public static Publication insert(Publication publication) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Publication detachedPublication=null;
		try {
			if(publication.getId() != null){ // เธ–เน�เธฒเน€เธ�เน�เธ� null assume เน�เธ”เน�เธงเน�เธฒ เน€เธ�เน�เธ� entity เน�เธซเธกเน� เน�เธกเน�เธ�เธฑเน�เธ�เธ•เน�เธญเธ�เน€เธ�เน�เธ�เธงเน�เธฒเธกเธตเธฃเธถเธขเธฑเธ�
				if (containsPublication(publication)) {
					throw new EntityExistsException("Publication entity already exists");
				}
			}

			Long now=System.currentTimeMillis()/1000L;
			
			// TODO: เธ•เธญเธ�เธ�เธตเน�เธขเธฑเธ�เน�เธกเน� support versioning fix เน�เธงเน� เน€เธ�เน�เธ� "v1" เธ�เธฑเธ� "true" เน�เธ�เธ�เน�เธญเธ�
			if(publication.getDateCreated()==null) // เธ�เธฃเธ“เธตเน�เธกเน� null เน�เธ�เธฅเธงเน�เธฒ เน�เธ�เน�เน€เธ�เธฅเธตเน�เธขเธ� version เน�เธซเธกเน�
				publication.setDateCreated(now);
			publication.setVersionDateCreated(now);
			publication.setType("textbook"); // TODO: textbook, or concept เน�เธ•เน�เธ•เธญเธ�เธ�เธตเน�เธกเธตเน�เธ�เธ�เน€เธ”เธตเธขเธงเน�เธ�เธ�เน�เธญเธ�
			if(publication.getVersion()==null) {
				publication.setVersion("v1");
				publication.setPublicationGroupId(now); // TODO: implement versioning capability with this id
			}
			
			publication.setStatus("in progress");
			publication.setIsLatestVersion(true); // assume เธงเน�เธฒเธ�เธฒเธฃ insert เน�เธซเธกเน�เธ�เธทเธญ version เน�เธซเธกเน�เธฅเน�เธฒเธชเธธเธ” (เน�เธ•เน�เธ•เธญเธ� update เน�เธฅเน�เธงเน�เธ•เน� user เธ•เธฑเน�เธ�เธ�เน�เธฒ เธ�เธถเน�เธ�เธชเธฒเธกเธฒเธฃเธ– set logic เธ�เธฑเธ”เธ�เธฒเธฃเน�เธงเน�เน�เธ�เธ�เธฑเน�เธ� client)
			
			mgr.makePersistent(publication);
			
			detachedPublication=mgr.detachCopy(publication);
			//My code//
			Long authorid = detachedPublication.getAuthorId();
			Author author = AuthorConnector.getById(authorid);
			author.setPublicationCount(author.getPublicationCount()+1);
			//====================================================//
		} finally {
			mgr.close();
		}
		
		return detachedPublication;
	}
	
	public static void remove(Long id) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Publication publication = null;
		try {
			publication = mgr.getObjectById(Publication.class, id);
			Long authorid = publication.getAuthorId();
			mgr.deletePersistent(publication);
			//My code//
			Author author = mgr.getObjectById(Author.class,authorid);
			author.setPublicationCount(author.getPublicationCount()-1);
			mgr.makePersistent(author);
			//==============================================//
		} finally {
			mgr.close();
		}
	}

	// TODO: If we want to protect some fields from modification, do this: fetch old entity by id and set new values from the entity parameter 
	public static Publication update(Publication publication) {
		PersistenceManager mgr = getPersistenceManager();
		Publication detachedPublication=null;
		try {
			if (!containsPublication(publication)) {
				throw new EntityNotFoundException("Publication entity does not exist");
			}
			mgr.makePersistent(publication);
			detachedPublication=mgr.detachCopy(publication);
		} finally {
			mgr.close();
		}
		return detachedPublication;
	}
	
	private static boolean containsPublication(Publication publication) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Publication.class, publication.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
