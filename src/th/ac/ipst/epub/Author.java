package th.ac.ipst.epub;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class Author 
{
    @PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    String identityProvider; // e.g. facebook, google, twitter
    Text userInfo; // user info in JSON obtained from identity provider
    String displayName;
    Text about;  // เกี่ยวกับผู้แต่ง
    Long dateOfMember; // วันที่สมัครเป็นสมาชิกระบบ
    Long publicationCount; // จำนวนหนังสืออิเล็กทรอนิกส์ที่เขียนขึ้นในระบบ
    String email; // ใช้เป็น user identity
    boolean emailVerified; // กันกรณี hacker ต้องการ impersonate
    
    /* TODO: เพิ่ม field พวกนี้ Twitter sign in ยังไม่ยอมให้ email หลัง sign in 
    // ต้องมี field พวกนี้ เพราะ Twitter ไม่ยอมให้ email เราเลยต้องจัดการ email verification process เอง!
    // และจำเป็นต้องใช้ twitter id ไว้ด้วย เพื่อตอนที่ user sign in ใหม่อีกครั้ง จะได้มีตัวผูก account ไว้ (ถ้าไม่ใช้ twitter id จะไม่รู้ว่า user ที่เข้ามาคือใคร)
    // http://stackoverflow.com/questions/10545507/how-to-verify-user-clicked-on-link-in-email-that-i-sent-him-her
	// http://stackoverflow.com/questions/5431061/how-send-automatic-reply-on-particular-email-id-when-an-user-registers
    String oneTimeHash;
    */
    
    public Long getId()
    {
    	return this.id;
    }
    public void setId(Long id)
    {
    	this.id=id;
    }
    public String getIdentityProvider()
    {
    	return this.identityProvider;
    }
    public void setIdentityProvider(String identityProvider)
    {
    	this.identityProvider=identityProvider;
    }
    public Text getUserInfo()
    {
    	return this.userInfo;
    }
    public void setUserInfo(Text userInfo)
    {
    	this.userInfo=userInfo;
    }
    public String getDisplayName()
    {
    	return this.displayName;
    }
    public void setDisplayName(String displayName) 
    {
    	this.displayName=displayName;
    }
    public Text getAbout()
    {
    	return this.about;
    }
    public void setAbout(Text about)
    {
    	this.about=about;
    }
    public Long getDateOfMember()
    {
    	return this.dateOfMember;
    }
    public void setDateOfMember(Long dateOfMember)
    {
    	this.dateOfMember=dateOfMember;
    }
    public Long getPublicationCount()
    {
    	return this.publicationCount;
    }
    public void setPublicationCount(Long publicationCount)
    {
    	this.publicationCount=publicationCount;
    }
    public String getEmail()
    {
    	return this.email;
    }
    public void setEmail(String email)
    {
    	this.email=email;
    }
    public boolean getEmailVerified()
    {
    	return this.emailVerified;
    }
    public void setEmailVerified(boolean emailVerified)
    {
    	this.emailVerified=emailVerified;
    }
    /*
    public String getOneTimeHash()
    {
    	return this.oneTimeHash;
    }
    public void setOneTimeHash(String oneTimeHash)
    {
    	this.oneTimeHash=oneTimeHash;
    }
    */
}
