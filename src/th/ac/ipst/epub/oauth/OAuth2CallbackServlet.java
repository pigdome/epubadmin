package th.ac.ipst.epub.oauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;

import th.ac.ipst.epub.Author;
import th.ac.ipst.epub.AuthorConnector;

@SuppressWarnings("serial")
public class OAuth2CallbackServlet extends HttpServlet {
	
	private static final Logger log=Logger.getLogger(OAuth2CallbackServlet.class.getName());

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

		/* TODO: error handling
		String error=req.getParameter("error");
		
		if(error.equals("access_denied")) {
			redirect to the original page or login page
		}
		*/		

		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");

		String verifierCode=req.getParameter("code");
		String state=req.getParameter("state");
		JSONObject stateJSON=null;
		try {
			stateJSON = (JSONObject)new JSONParser().parse(state);
		} catch (ParseException e1) { e1.printStackTrace(); }
		String provider =(String)stateJSON.get("provider");
		String security_token=(String)stateJSON.get("security_token");

		if(verifierCode==null ||
		   !req.getSession().getAttribute("security_token").equals(security_token)) {

			resp.getWriter().println("Invalid parameter");
			resp.setStatus(401);	
			
			return;
		}
		JSONObject userInfo=null;
		String email=null;
		String identityProvider=null;
		String displayName=null;
		boolean emailVerified=false;

		if(provider.equals("google")) {

				// เธซเธฅเธฑเธ�เธ�เธฒเธ� sign-in เธ”เธถเธ�เธ�เน�เธญเธกเธนเธฅเน�เธ�เน�เธ�เธฃเน�เธ�เธฅเน�เธ�เธญเธ� user
				try {
					userInfo = (JSONObject)new JSONParser().parse(GoogleOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)userInfo.get("email");
				displayName=(String)userInfo.get("name");
				identityProvider="google";
				emailVerified=(Boolean)userInfo.get("verified_email");
				
		} else if(provider.equals("facebook")) {

				// เธซเธฅเธฑเธ�เธ�เธฒเธ� sign-in เธ”เธถเธ�เธ�เน�เธญเธกเธนเธฅเน�เธ�เน�เธ�เธฃเน�เธ�เธฅเน�เธ�เธญเธ� user
				try {
					userInfo = (JSONObject)new JSONParser().parse(FacebookOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)userInfo.get("email");
				displayName=(String)userInfo.get("name");
				identityProvider="facebook";
				emailVerified=(Boolean)userInfo.get("verified");

		} else if(provider.equals("microsoft")) {

				// เธซเธฅเธฑเธ�เธ�เธฒเธ� sign-in เธ”เธถเธ�เธ�เน�เธญเธกเธนเธฅเน�เธ�เน�เธ�เธฃเน�เธ�เธฅเน�เธ�เธญเธ� user
				try {
					userInfo = (JSONObject)new JSONParser().parse(MicrosoftOAuthHelper.getInstance().getUserInfoForVerifierCode(verifierCode));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				email=(String)((JSONObject)userInfo.get("emails")).get("account");
				displayName=(String)userInfo.get("name");
				identityProvider="microsoft";
				emailVerified=true; // microsoft forces users to verify email prior to OAuth2.0 use
		} 
		// my code
		else {
			try {
				userInfo = (JSONObject) new JSONParser().parse(req.getParameter("author"));
				System.out.print(userInfo);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			email=(String)userInfo.get("email");
			displayName=(String)userInfo.get("name");
			identityProvider="none";
			emailVerified=true;
		}
		//========================================//
		if(!emailVerified) {

			resp.sendRedirect("/email-verification-needed.html");
			
			return;
		}
		
		// เน€เธ�เน�เธ�เธงเน�เธฒเน€เธ�เธขเธฅเธ�เธ—เธฐเน€เธ�เธตเธขเธ�เน€เธ�เน�เธฒเธฃเธฐเธ�เธ�เน�เธ�เธฃเธถเธขเธฑเธ� (เน�เธ�เน� email เธ�เน�เธ�เธซเธฒ)
		Author a=AuthorConnector.getByEmail(email); 

		// เธ�เธฃเธ“เธตเธ—เธตเน�เน€เธ�เน�เธ�เธชเธกเธฒเธ�เธดเธ�เน�เธซเธกเน� เน�เธซเน�เธชเธฃเน�เธฒเธ� entity Author เน�เธซเธกเน�เน€เธฅเธข เธ�เธฃเน�เธญเธกเน�เธชเน�เธ�เน�เธญเธกเธนเธฅเน€เธ—เน�เธฒเธ—เธตเน�เธฃเธนเน�เน€เธ�เน�เธฒเน�เธ�
		boolean newRegistration=false;
		if(a==null) {
			newRegistration=true;
			a=new Author();
			a.setEmail(email);
			a.setEmailVerified(emailVerified); // เน€เธ�เธทเน�เธญเธ•เธฒเธก IdP เน€เธฅเธข
			a.setIdentityProvider(identityProvider);
			a.setDisplayName(displayName);
			a.setUserInfo(new Text(userInfo.toJSONString()));
			AuthorConnector.insert(a);
		} 
		
		if(newRegistration) 
			req.getSession().setAttribute("newMember", Boolean.TRUE);
		else
			req.getSession().setAttribute("newMember", Boolean.FALSE);
		req.getSession().setAttribute("authorDisplayName", a.getDisplayName());
		req.getSession().setAttribute("authorId", a.getId());
		
		String action=(String)stateJSON.get("action");
		resp.setContentType("text/html;charset=UTF-8");
		 if(action!=null && action.equals("close-after-logged-in")) { // generate เธซเธ�เน�เธฒ upload เธ�เธฒเธ�เธ�เธฑเน�เธ� server เน€เธ�เธทเน�เธญเธ�เธฑเธ� unauthorized accesss
			resp.sendRedirect("/close-myself.html");
		 } else {
			// resp.sendRedirect("/"); // เธ—เธณเน�เธ�เธ�เธ�เธตเน�เน�เธ”เน�เน€เธ�เธฃเธฒเธฐเน�เธชเน� index.jsp เน�เธ� welcome-file เน�เธฅเน�เธง (เธญเธขเธนเน�เน�เธ� web.xml)
			resp.sendRedirect("http://ipst-epub.appspot.com/"); // IMPORTANT: เน�เธ�เน�เน�เธ�เธ�เธ”เน�เธฒเธ�เธ�เธ�เน�เธกเน�เน�เธ”เน� เน€เธ�เธฃเธฒเธฐเธซเธฅเธฑเธ�เธ�เธฒเธ� oauth เน€เธชเธฃเน�เธ�เธกเธฑเธ�เธ�เธฐ redirect เน�เธ”เธขเน�เธ�เน� protocol https เธ”เธฑเธ�เธ�เธฑเน�เธ�เธ•เน�เธญเธ�เธ�เธณเธซเธ�เธ” absolute url เน�เธ�เน€เธฅเธข (TODO: เธ–เน�เธฒเธ�เธฐเน€เธ�เธฅเธตเน�เธขเธ� domain เธ—เธตเธซเธฅเธฑเธ� เธญเธขเน�เธฒเธฅเธทเธกเธกเธฒเน�เธ�เน�เธ•เธฃเธ�เธ�เธตเน�เธ”เน�เธงเธข!)
		}

    }
}
