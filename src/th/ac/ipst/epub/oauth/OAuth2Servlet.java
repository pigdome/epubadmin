package th.ac.ipst.epub.oauth;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.appengine.api.datastore.Text;

import th.ac.ipst.epub.Author;
import th.ac.ipst.epub.AuthorConnector;

import java.util.logging.Logger;

@SuppressWarnings("serial")
public class OAuth2Servlet extends HttpServlet {
	
	private static final Logger log=Logger.getLogger(OAuth2Servlet.class.getName());

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

		// https://developers.google.com/accounts/docs/OAuth2Login
		// Create a state token to prevent request forgery.
		// Store it in the session for later validation.
		String security_token = new BigInteger(130, new SecureRandom()).toString(32);
		req.getSession().setAttribute("security_token", security_token);
		
		String provider=req.getParameter("provider");
		String action=req.getParameter("action"); // upload, login, etc. เธ•เน�เธฒเธ�เธ�เธฑเธ�เธ—เธตเน�เธ–เน�เธฒ upload เธ�เธฐ gen upload form เธ�เธฒเธ� server เน�เธซเน�เน€เธฅเธข
		
	    // เธชเน�เธ�เธ�เน�เธฒเธ� JSON เธ�เธตเน�เน�เธ�เน�เธ� URL, identity provider เธ�เธฐเธชเน�เธ�เธ�เธฅเธฑเธ�เธกเธฒเธญเธตเธ�เธ—เธตเน�เธ� callback เน€เธ�เธทเน�เธญเธ—เธณ validation (i.e., security_token เธงเน�เธฒ match เธ�เน�เธฒเธ—เธตเน�เน€เธ�เน�เธ�เน�เธงเน�เน�เธ� session เธฃเธถเน€เธ�เธฅเน�เธฒ เน�เธฅเธฐเน�เธซเน�เน€เธ�เน�เธฒ logic เธ•เน�เธฒเธ�เธ�เธฑเธ�เธชเธณเธซเธฃเธฑเธ�เน�เธ•เน�เธฅเธฐ provider)
	    // เธ•เน�เธญเธ�เธชเน�เธ�เธ�เน�เธฒเธ� parameter "state" (เธ”เธน http://stackoverflow.com/questions/6463152/facebook-oauth-custom-callback-uri-parameters)
		JSONObject statesJSON=new JSONObject(); 
	    statesJSON.put("security_token", security_token);
	    statesJSON.put("action", action);

		if(provider.equals("google")) {
		    statesJSON.put("provider", "google");	// เธชเธณเธซเธฃเธฑเธ� implement logic เธ•เน�เธฒเธ�เธ�เธฑเธ� เน�เธ� callback	    
		    res.sendRedirect(GoogleOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));
		}
		else if(provider.equals("facebook")) {
		    statesJSON.put("provider", "facebook");	// เธชเธณเธซเธฃเธฑเธ� implement logic เธ•เน�เธฒเธ�เธ�เธฑเธ� เน�เธ� callback	    
			res.sendRedirect(FacebookOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));
		}
		else if(provider.equals("microsoft")) {
		    statesJSON.put("provider", "microsoft");	// เธชเธณเธซเธฃเธฑเธ� implement logic เธ•เน�เธฒเธ�เธ�เธฑเธ� เน�เธ� callback	    
			res.sendRedirect(MicrosoftOAuthHelper.getInstance().getAuthorizationUrlWithJSONParameters(statesJSON));			
		}
		else{
			JSONObject userInfo=null;
			String email=null;
			String identityProvider=null;
			String displayName=null;
			boolean emailVerified = false;
			try {
				userInfo = (JSONObject) new JSONParser().parse(req.getParameter("author"));
				System.out.print(userInfo);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			email=(String)userInfo.get("email");
			displayName=(String)userInfo.get("name")+"  "+userInfo.get("surname");
			identityProvider="none";
			emailVerified=true;
			Author a=AuthorConnector.getByEmail(email); 
			boolean newRegistration=false;
			if(a==null) {
				newRegistration=true;
				a=new Author();
				a.setEmail(email);
				a.setEmailVerified(emailVerified); // เน€เธ�เธทเน�เธญเธ•เธฒเธก IdP เน€เธฅเธข
				a.setIdentityProvider(identityProvider);
				a.setDisplayName(displayName);
				a.setUserInfo(new Text(userInfo.toJSONString()));
				AuthorConnector.insert(a);
			} 
			
			if(newRegistration) 
				req.getSession().setAttribute("newMember", Boolean.TRUE);
			else
				req.getSession().setAttribute("newMember", Boolean.FALSE);
			req.getSession().setAttribute("authorDisplayName", a.getDisplayName());
			req.getSession().setAttribute("authorId", a.getId());
			res.setContentType("text/html");
	        res.setCharacterEncoding("UTF-8");
		    res.getWriter().write("sdf");
		}
    }
}
