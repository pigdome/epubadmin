package th.ac.ipst.epub.oauth.scribe.example;

import java.util.*;

import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;

// http://stackoverflow.com/questions/9766771/using-live-connect-api-in-asp-net-to-retrieve-a-users-email-address
// https://github.com/eranation/scribe-java-constantcontact-oauth2/blob/master/src/main/java/org/scribe/builder/api/LiveApi.java
public class MicrosoftExample
{
  private static final String NETWORK_NAME = "Microsoft";
  private static final String PROTECTED_RESOURCE_URL = "https://apis.live.net/v5.0/me";
  private static final Token EMPTY_TOKEN = null;
  private static final String SCOPE="wl.emails"; // http://msdn.microsoft.com/en-us/library/live/hh243646.aspx

  public static void main(String[] args)
  {

    // Replace these with your own api key and secret
    String apiKey = "change this";
    String apiSecret = "change this";
    
    OAuthService service = new ServiceBuilder()
                                  .provider(LiveApi.class)
                                  .apiKey(apiKey)
                                  .apiSecret(apiSecret)
                                  .callback("https://ipst-epub.appspot.com/oauth2callback")
                                  .scope(SCOPE)
                                  .build();
    Scanner in = new Scanner(System.in);

    System.out.println("=== " + NETWORK_NAME + "'s OAuth Workflow ===");
    System.out.println();

    // Obtain the Authorization URL
    System.out.println("Fetching the Authorization URL...");
    
    String authorizationUrl = service.getAuthorizationUrl(EMPTY_TOKEN);
    //String authorizationUrl = FacebookOAuthHelper.getInstance().getAuthorizationUrlForSecurityToken("secured");
    
    System.out.println("Got the Authorization URL!");
    System.out.println("Now go and authorize Scribe here:");
    System.out.println(authorizationUrl);
    System.out.println("And paste the authorization code here");
    System.out.print(">>");
    Verifier verifier = new Verifier(in.nextLine());
    System.out.println();
    
    // Trade the Request Token and Verfier for the Access Token
    System.out.println("Trading the Request Token for an Access Token...");
    Token accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
    System.out.println("Got the Access Token!");
    System.out.println("(if your curious it looks like this: " + accessToken + " )");
    System.out.println();

    // Now let's go and ask for a protected resource!
    System.out.println("Now we're going to access a protected resource...");
    OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
    service.signRequest(accessToken, request);
    Response response = request.send();
    System.out.println("Got it! Lets see what we found...");
    System.out.println();
    System.out.println(response.getCode());
    System.out.println(response.getBody());

    System.out.println();
    System.out.println("Thats it man! Go and build something awesome with Scribe! :)");
	  

  }
}