package th.ac.ipst.epub.oauth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * A helper class for Google's OAuth2 authentication API.
 * @version 20130224
 * @author Matyas Danter
 */

// Modified from http://ocpsoft.org/java/setting-up-google-oauth2-with-java/ by Pao 

public final class GoogleOAuthHelper {

	private final static GoogleOAuthHelper instance = new GoogleOAuthHelper();

	private String clientId;
	private String clientSecret;
	private String userInfoUrl;
	private String redirectUri;
	private Iterable<String> scopes;
	
	private final JsonFactory JSON_FACTORY = new JacksonFactory();
	private final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	
	private GoogleAuthorizationCodeFlow flow;
	
	private GoogleOAuthHelper() 
	{
		JSONObject secretsDictionary=null;
		try {
			secretsDictionary = (JSONObject)new JSONParser().parse(Resources.toString(Resources.getResource("google_client_secrets.json"), Charsets.UTF_8));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject webSecretsDictionary=(JSONObject)secretsDictionary.get("web");
		clientId=(String)webSecretsDictionary.get("client_id");
		clientSecret=(String)webSecretsDictionary.get("client_secret");
		redirectUri=(String)webSecretsDictionary.get("redirect_uri");
		userInfoUrl=(String)webSecretsDictionary.get("user_info_url");
		String scopesString=(String)webSecretsDictionary.get("scopes");
		scopes=Arrays.asList(scopesString.split(";"));

		flow=new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientId, clientSecret, (Collection<String>) scopes)
			.setAccessType("online") // Sets the access type ("online" to request online access or "offline" to request offline access) or null for the default behavior of "online". By default this has the value "offline".
			.setApprovalPrompt("auto") // Sets the approval prompt behavior ("auto" to request auto-approval or "force" to force the approval UI to show) or null for the default behavior of "auto". By default this has the value "force".
			.build();
	}

	@SuppressWarnings("unchecked")
	public String getAuthorizationUrlWithJSONParameters(JSONObject statesJSON) {
		
	    // à¸ªà¹ˆà¸‡à¸œà¹ˆà¸²à¸™ JSON à¸™à¸µà¹‰à¹„à¸›à¹ƒà¸™ URL, identity provider à¸ˆà¸°à¸ªà¹ˆà¸‡à¸�à¸¥à¸±à¸šà¸¡à¸²à¸­à¸µà¸�à¸—à¸µà¹ƒà¸™ callback à¹€à¸žà¸·à¹ˆà¸­à¸—à¸³ validation (i.e., security_token à¸§à¹ˆà¸² match à¸„à¹ˆà¸²à¸—à¸µà¹ˆà¹€à¸�à¹‡à¸šà¹„à¸§à¹‰à¹ƒà¸™ session à¸£à¸¶à¹€à¸›à¸¥à¹ˆà¸² à¹�à¸¥à¸°à¹ƒà¸«à¹‰à¹€à¸‚à¹‰à¸² logic à¸•à¹ˆà¸²à¸‡à¸�à¸±à¸™à¸ªà¸³à¸«à¸£à¸±à¸šà¹�à¸•à¹ˆà¸¥à¸° provider)

		final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
		
		return url.setRedirectUri(redirectUri).setState(statesJSON.toJSONString()).build();
	}
	
	public String getUserInfoForVerifierCode(String code) throws IOException
	{
		GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectUri).execute();
		Credential credential = flow.createAndStoreCredential(response, null); // userId - user ID or null if not using a persisted credential store
		HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
		// Make an authenticated request
		GenericUrl url = new GenericUrl(userInfoUrl);
		HttpRequest request = requestFactory.buildGetRequest(url);
		request.getHeaders().setContentType("application/json");
		return request.execute().parseAsString();
	}

	public static GoogleOAuthHelper getInstance() { return instance; }
}
