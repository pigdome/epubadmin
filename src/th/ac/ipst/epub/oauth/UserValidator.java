package th.ac.ipst.epub.oauth;

import javax.servlet.http.HttpSession;

public class UserValidator {
	
	public UserValidator() {
		
	}
	
	public boolean validate(HttpSession session)
	{
		Long authorId=(Long)session.getAttribute("authorId");
		if(session==null || authorId==null)
			return false;
		else 
			return true;
	}

}
