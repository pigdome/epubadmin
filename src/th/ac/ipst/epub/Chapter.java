package th.ac.ipst.epub;

import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable="true")
public class Chapter 
{
	@PrimaryKey
    @Persistent ( valueStrategy =  IdGeneratorStrategy. IDENTITY )
    Long id;

    Long publicationId;
    //String version; �����繵�ͧ��� version ���� query ��ҡ publicationId ����� key �ͧ Publication (���� version �ͧ˹ѧ����������ǡѹ�� publicationId ����ӡѹ ��ҧ�ҡ publicationGroupId ��觫�ӡѹ����Ѻ˹ѧ����������ǡѹ���ҧ version)
    
    // information about <package />
    String title = "untitled"; 
    String identifier = "NO_ID";
    String language = "th"; 
    
    public Chapter(String id,String userEnterTitle){
    	publicationId = Long.parseLong(id);
    	title = userEnterTitle;
    }
    public Long getId()
    {
    	return this.id;
    }

    public void setId(Long id)
    {
    	this.id=id;
    }
    
    public Long getPublicationId()
    {
    	return this.publicationId;
    }

    public void setPublicationId(Long publicationId)
    {
    	this.publicationId=publicationId;
    }
    
    public String getTitle()
    {
    	return this.title;
    }

    public void setTitle(String title)
    {
    	this.title=title;
    }
}
