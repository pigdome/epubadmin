package th.ac.ipst.epub;

import java.util.HashMap;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

public class AuthorConnector {

	private static final Integer DEFAULT_LIMIT = 25;
	
	@SuppressWarnings("unchecked")
	public static List<Author> list(String cursorString, Integer limit, String order) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Author> authors = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Author.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
        	
    	    if (order != null) {
    	    	if (order.equals("new")) {
    	    		query.setOrdering("dateOfMember desc");
    		    } else if (order.equals("publication-count")) {
    		        query.setOrdering("publicationCount desc");
    		    }
    	    } else {
	    		query.setOrdering("dateOfMember desc");
    	    }

			authors = (List<Author>) query.execute();
			cursor = JDOCursorHelper.getCursor(authors);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

		} finally {
			mgr.close();
		}
		
		return authors;
	}
	//My code//
	public static List<Author> list(Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Author> authors = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Author.class);
			if   ( limit == null )   {
            	limit=DEFAULT_LIMIT;
            }
        	query. setRange ( 0 , limit ) ;
	    	query.setOrdering("dateOfMember desc");
			authors = (List<Author>) query.execute();
		} finally {
			mgr.close();
		}
		
		return authors;
	}
	//==============================================================//
	
	public static Author getById(Long id) {

		PersistenceManager mgr = getPersistenceManager();
		Author author = null;

		try {
			author = mgr.getObjectById(Author.class, id);
		} finally {
			mgr.close();
		}
		return author;		
	}
	
	@SuppressWarnings("unchecked")
	public static Author getByEmail(String email) 
	{
		PersistenceManager mgr = getPersistenceManager();
		Author author = null;
		try {
          Query query =  mgr. newQuery ( Author. class ) ;
          query.setFilter("email==emailParam");
          query.declareParameters("String emailParam");
          List<Author> results = (List<Author>) query.execute(email);
          if(results.size()>0) {
          	author=results.get(0);
          }
          
		} finally {
			mgr.close();
		}
		
		return author;		
	}
	
	public static void insert(Author author) 
	{
		PersistenceManager mgr = getPersistenceManager();
		try {
			if(author.getId() != null){ // เธ–เน�เธฒเน€เธ�เน�เธ� null assume เน�เธ”เน�เธงเน�เธฒ เน€เธ�เน�เธ� entity เน�เธซเธกเน� เน�เธกเน�เธ�เธฑเน�เธ�เธ•เน�เธญเธ�เน€เธ�เน�เธ�เธงเน�เธฒเธกเธตเธฃเธถเธขเธฑเธ�
				if (containsAuthor(author)) {
					throw new EntityExistsException("Author entity already exists");
				}
			}
			
			//author.setDateOfMember(System.currentTimeMillis()/1000L);
			author.setDateOfMember(System.currentTimeMillis()); //why wee need to devide by 1000L
			author.setPublicationCount(0L);

			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
	}
	
	/* TODO: เน€เธ�เน�เธ�เธงเน�เธฒ update เน�เธ�เธ�เธ�เธตเน� work เธฃเธถเน€เธ�เธฅเน�เธฒ เธ�เธณเน�เธ”เน�เธงเน�เธฒ เธ•เน�เธญเธ� get เธญเธญเธ�เธกเธฒ (เธ�เธฒเธ� author.getId()) เน�เธฅเน�เธง set เธ�เน�เธฒเธ�เธฅเธฑเธ�เน€เธ�เน�เธฒเน�เธ� เธ�เธฃเธดเธ�เน�เน�เธฅเน�เธง เน�เธ�เธ�เธ�เธตเน�เธ�เธฐ safe เธ�เธงเน�เธฒเธ”เน�เธงเธข เน€เธ�เธฃเธฒเธฐเธ�เธฑเธ�เธ�เธฑเธ�เน�เธ”เน�เธงเน�เธฒ field เน�เธซเธ�เน€เธ�เธฅเธตเน�เธขเธ�เน�เธ”เน�เธ�เน�เธฒเธ� ignore field เน�เธซเธ�เธ�เน�เธฒเธ� (เน�เธ�เน�เน€เธชเธตเธขเน€เธงเธฅเธฒ get entity เน€เธ�เน�เธฒเธญเธญเธ�เธกเธฒเธ�เน�เธญเธ�)*/
	public static Author update(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsAuthor(author)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(author);
		} finally {
			mgr.close();
		}
		return author;
	}
	
	
	private static boolean containsAuthor(Author author) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Author.class, author.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
	//public static Author update(Author author) {
		// TODO Auto-generated method stub
	//	return null;
	//}
	public static void remove(Long authorId) {
		PersistenceManager mgr = getPersistenceManager();
		Author author = null;
		try {
			author = mgr.getObjectById(Author.class, authorId);
			mgr.deletePersistent(author);
			Query query = mgr.newQuery(Publication.class);
			query.setFilter("authorId==authorIdParam" );
            query.declareParameters("Long authorIdParam");
            
			List<Publication> publications = (List<Publication>) query.execute(authorId);
			int index =publications.size()-1;
			while(index>=0)
			{
				PublicationConnector.remove(publications.get(index).getId());
				index--;
			}
		} finally {
			mgr.close();
		}
	}
}
